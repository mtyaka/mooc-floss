# Project management - Meeting 1

Video: https://digimedia1.r2.enst.fr/playback/presentation/2.0/playback.html?meetingId=73d3cd53ec8ede3f636b9a782ecc9f6375cf2271-1617642008362

S1: 00:00:06.078 I made the record of the last week public [inaudible].

S2: 00:00:09.917 Great. So I'll start the--

S1: 00:00:11.973 I still have to push a button to do that, but.

S2: 00:00:15.467 Sounds good. Yeah, the earlier you do it, basically, the earlier I start the transcription which can be useful, but no rushing there. It still takes a few days, anyway, for the transcription to happen.

S1: 00:00:30.271 Yeah. As for Remi and I, we did have some exchanges with our administration, and they wanted to discuss in-depth what we want to do with the MOOC and how they want to discuss with other people around the MOOC because they are very much not used to discussing with-- well, to not do stuff alone, basically.

S2: 00:01:05.772 As most organizations are. Yeah?

S1: 00:01:08.938 Yeah, especially a public one.

S2: 00:01:11.966 Which is crazy. But, yeah, whatever.

S1: 00:01:16.166 So I think it's this week or next week, and then we will be able to go back to you, Xavier. Let's see. It's tomorrow, actually.

S2: 00:01:31.311 Oh, all right.

S1: 00:01:32.766 Yeah. So tomorrow, we have a meeting, and then we can set up a meeting with you.

S2: 00:01:38.726 Sounds good. Yeah. To a [public?] context of that [inaudible]. We need to still talk about the business side of things because, originally, this was an IMT MOOC with Open edX. We arrived because we were planning to do the same thing. At the same time, there are different consideration. Between edX themselves will be publishing the MOOC. IMT will definitely want to be able to get some money back from their investment around-- like people would get their certificate on edx.org. Those kind of things. On our side, too, we are counting, maybe, around mentoring or something like that. And, obviously, if you want to be involved in those discussions, to have the same thing around OpenStack. That could also be a good thing to do. So basically, hopefully, there shouldn't be any blockers. But it's a matter of all agreeing at the beginning, so that we don't end up with surprise later on.

S1: 00:02:51.243 Yeah. if you want to be part of the discussions about finance side of the project, you are very welcome to join, of course.

S3: 00:03:04.404 I think I'm good for right now, but I'll keep in the back of my mind for later.

S2: 00:03:12.224 I was kind of expecting that answer, but I just wanted to make sure you felt welcome to it, so.

S3: 00:03:17.415 Oh, yeah. I appreciate the invitation. I have plenty of meetings on my calendar already. Thank you. Don't worry.

S2: 00:03:24.952 Yeah. Yeah. No worries.

S1: 00:03:26.070 It has chances to be more boring than the subject of the MOOC itself.

S2: 00:03:31.331 Yes. It's not the one I'm, yeah, anticipating with the most trepidation for sure.
[silence]

S2: 00:03:48.951 All right. So how do we do that? Kendall, you had some specific things you wanted to cover. Maybe already, we could start with that.

S3: 00:03:55.454 So does somebody have the link to the spreadsheet-looking thing that we were using last week?
I want to say EtherCalc, but I know it's not EtherCalc. But it really looks like EtherCalc.

S1: 00:04:05.903 It is an EtherCalc instance. It's Framacalc I think.

S3: 00:04:09.196 Okay. Yeah. I figured we would just walk through that, and then we could set up next-- or week twos or whatever and then call it--

S2: 00:04:22.056 By the way I've turned the content of that Calc already into a Markdown file to have the structure that we have kind of established last week, and to start manipulating it. So basically one of my pull requests from yesterday is taking the structure and starting to add the content that we had from the first brainstorming session - the one actually before you joined - so that this way we have that. Haven't done the second one, so the one from last week because we don't have the transcript yet. But just to get that started and to make sure we have the raw materials kind of basically easily available.

S3: 00:05:02.246 Cool. So I would say we just go down the list for the person in charge of the things, like have we made progress on these things? If there are blocking things, we can maybe help with that. Yeah, I don't know. I'm just throwing ideas out there.

S2: 00:05:30.914 No, no. I think that's a great thing to do to make sure that we follow up on the things we commit on, definitely. So, Marc, do you want to lead that? How do you want to do this?

S1: 00:05:53.709 So we do in order, basically. So Remi is not here and Loic is not here so I'm--

S2: 00:06:01.486 Oh, yeah. Loic is not here. Let me ping him because he skipped last week. Oh, he assumes that it's not the case because it's Easter. But I'm going to tell him that, yes, we are meeting, but yeah.

S3: 00:06:16.806 We're dedicated.

S2: 00:06:22.086 But, yeah, we can skip that I guess for now.

S1: 00:06:25.475 But I did not do anything specific for this part during last week.

S3: 00:06:32.586 Okay. No worries. I guess, skipping down the list. So I spent some time coming up with various open source FOSS names that I know of and what areas they-- or what projects work on. And I put that list in the issue for-- it was an issue that Xavier had made.

S2: 00:07:08.158 Yup, I saw that. Yeah, we used that list and the previous names that were on the ticket. And I've actually added that to the sections around interviews in the Markdown document that I mentioned before. Actually, maybe I'll copy those. This way you can see what I'm talking about.

S3: 00:07:29.506 Cool. Here.

S2: 00:07:33.185 Let's see. That's here.

S1: 00:07:35.353 Issue 12, right?

S2: 00:07:38.161 That might be it. So I'm looking more for the merge request. So if you have the issue, maybe copy that already. Yes, and the pull request that I've done is this one. So basically based on what I was mentioning earlier, where I created that Markdown with the structure from this spreadsheet. Then in this section, you can see I've added the ones that you've mentioned. And I think I also added things like Microsoft, Google which were mentioned as an idea in the transcript, just make sure that we have all the ideas so far. Doesn't mean that it has to be the list, but it's just at least we have everything in one place. And then when we get to actually contacting people, we can maybe groom that list a bit more. I haven't removed Linus Torvalds. You'll see I've replied to you on that on the ticket. But obviously it's something we can discuss over time.

S3: 00:08:47.189 Yeah. Cool. Looks like you have a set of questions to talk to them.

S2: 00:08:58.734 Yes. Same thing. Mostly we use the ones that were mentioned in things. So this is not wrong. It's more on like a compilation of the brainstorming so far.

S3: 00:09:11.015 Cool. Yeah. I think it's best to just collect everything. And then when we get to activating on it, we can groom things a little bit more and whatnot. I can also work on coming up with some questions to ask. I had focused on list of people first. And, yeah, if I come up with more people, I will definitely either add on to your patch, or I'll just throw them an issue again too.

S2: 00:09:49.398 Sure. You can even open a pull request either against what I did or once it's merged, directly against the code itself-- I mean the code, which is kind of the code for us, the text file. This way, we'll have kind of a central place for those things.

S3: 00:10:09.329 Yeah. Sounds good. Yeah. So we can work on that between now and next week.

S2: 00:10:16.526 And also not to go too much of tangent with this. But I would look maybe also into the pull request and do some comment. For example, one of the points that I've mentioned is that it could be useful to-- actually it's not on this pull request. It's on the previous one that recaps kind of what we had said so far, is that interviews might be good beyond just this section. I think like it can be interesting to have like little excerpts or point of views in the various parts of the course. So what I'm thinking is that the list of question that we have are here, we can probably add also other questions that come from all other parts of the course progressively as we work through it. And at the end, we'll have a big questionnaire that we can trim down, and maybe try to have material a bit for all sections of the course, and then use it a little bit there and a little bit there rather than having people watching a long series of lengthy interviews, which probably they wouldn't want to do so.

S3: 00:11:27.422 Right. Yeah. I think that's a good plan. I like the idea of presenting people with a bigger list of questions, and then letting them answer a subset of them that they are more passionate about. Because I think we'll get better responses rather than being like, "Answer all 50 questions," or whatever.

S2: 00:11:49.766 Yes. Definitely. Oh, hello Loic. Cool. Cool that you can join. Sorry to bother you on Easter day. Yeah.

S4: 00:12:02.307 And for the questions, as we are progressing, maybe there are some topics that come up during the discussion, so we can always grow the list of questions based on what we went into with each people.

S2: 00:12:17.897 Often in an interview, the best parts come when the things flow a little bit and people start talking more freely. So, yeah, I agree with that.

S3: 00:12:27.615 Yeah, I think we wait till we've had our individual meetings on each of the modules and collected a bigger list of questions to activate on those interviews.

S2: 00:12:43.563 Yeah. I think it might almost be one of the last thing to do in the course because then we'll really know what we want to have on top of it. We might also have a bit more to show to people so that they are more interested in actually participating to the course and this kind of thing, so.

S3: 00:13:00.367 Yeah, makes sense.

S1: 00:13:04.970 At this point, they could also add their questions to the list of questions to ask the other interviewees.

S2: 00:13:13.634 That is true if they are motivated.

S3: 00:13:21.783 Well, we made progress on our thing. [laughter]

S2: 00:13:28.033 Yes, and not you, Marc. That's really bad. [laughter]

S1: 00:13:33.566 Yeah, sorry. I have now one more week to do stuff. [laughter]

S3: 00:13:39.600 Yeah, okay, we weren't-- today was kind of a, I don't know, a wild card in that it was right after a holiday if we were going to meet or not, so definitely would not feel bad if I were you. I just got into a very procrastinate-y, I don't want to do my normal work, on whatever day it was that I worked on that, Friday, Thursday, something like that. And so that was why I made progress like I did.

S2: 00:14:09.424 Yeah, that's one of the best thing, is to use procrastination to do something else on which we would probably procrastinate. I use that technique a lot too.

S3: 00:14:17.126 Oh yeah, it's really good. [laughter]

S1: 00:14:19.190 But I have some urgent work that's coming, so I would probably advance on the MOOC instead.

S2: 00:14:25.130 Perfect, you got it.

S3: 00:14:26.727 Exactly. Yeah. Okay, so in terms of other people's work, we had Loic, you were listed for the historical perspective on the four freedoms and stuff.

S2: 00:14:45.422 Oh yeah, it's true. Loic, you weren't there last time, so we assigned you everywhere. I hope you're okay with that? [laughter] So, yeah, do you want to actually review the points on which we assigned you to? That's why you--

S1: 00:15:02.734 So that you can refuse or confirm that you're interested in it.

S2: 00:15:05.261 Yes. Exactly. Sorry, I hope we are not taking you out of family dinner or something like that because I see that you're escaping.

S5: 00:15:19.690 I have to make not too much noise, that's why, but yeah, it's not family dinner. Yeah, I'm actually reviewing the document indeed.

S1: 00:15:37.209 So the two points where we assigned you are the Minetest activity and the historical perspective on the FLOSS movement.

S5: 00:15:51.476 This is on which-- so I was reviewing the brainstorm.md.

S1: 00:15:58.130 Oh, it's under Framacalc. Sorry. One of the links. The first link in [inaudible].

S5: 00:16:11.371 The first one. Okay. Yeah.

S1: 00:16:13.948 So Framacalc.

S5: 00:16:14.674 Found it. [inaudible]. Yes, I have it.

S1: 00:16:21.665 So basically, last week, we started from the brainstorm and from the transcript and tried to make it into a detailed list of what the students will go through during the MOOC in this module. And so it is also a list of the work to do for this module. And we made a command person in charge so that we have people who know what to do.

S5: 00:17:00.655 Okay. I'm good, in general, to do that. The time frame is good. When will it be?

S2: 00:17:13.102 So you have to do that for right now. Is that okay?

S1: 00:17:20.636 There is no firm deadline, just to be done, in general. And to be done before the person in charge for the whole week comes to you and say, "Hey, did you progress on that?"

S5: 00:17:37.376 Okay. I don't have the big picture. I'm happy to provide a few hours of work if you tell me when. Though I wouldn't be technically in charge. I would be doing groundwork, legwork. I'm good with that. So for the Minetest, of course. Oh, so it's an interview. 5, 10 minutes. I'm happy to be interviewed, if that's the goal.

S1: 00:18:19.332 You could be interviewed.

S5: 00:18:21.860 Or if it's to interview someone.

S2: 00:18:27.480 I'm not sure what was the plan, but I think we don't need to really have the final contents for all those things. It's more like iterating over it and trying to put thing together. One thing that you can have a look at is the-- well, the pull request, I pasted the 13 one because that's where, like I was mentioning earlier, I've put that structure from the spreadsheet into a Markdown file. And within that structure, I've copied elements from the previous brainstorming, especially the transcripts. So the next step, I think, could be to work on those sections and try to feel a bit what should be the content there. Maybe not the finalized one, but getting a fairly good idea of what would be the thing that could be said, how they would be said, maybe start to have a draft of the script of what the person would be saying, or if it's an interview, what the question would be, who they would be asked, because right now, your questions basically have no answers. We don't really know what we have in there. But there is some content from the previous brainstorming meetings that can already provide some context.

S3: 00:19:52.595 I feel like part of it--

S5: 00:19:53.180 Okay. Okay, the--

S3: 00:19:54.459 Oh, sorry.

S5: 00:19:56.337 Yeah, sorry. Go ahead.

S3: 00:19:58.968 I feel like it would be good to have some interview aspect to it and then also, some written text to go with it. If you have questions that you would want included in a list of interviews, then adding them on to Xavier's pull request would be good. Otherwise, yeah, I think that I am generally going to push towards having as much content written down aside from interviews, just because it's much easier for people that don't speak English to do the translations and stuff, and it's easier to work through it at your pace, rather than having to go back, rewatch the video like two, three, four times and whatnot. So I think wherever we can, we should write things down because that was the direction that we had moved with our Upstream Institute, formerly Upstream University, because we see better engagement and retention of the information and that sort of thing when people have to read it. And yeah, it's easier to update down the road because updating videos is a pain. So, yeah, wherever we can, I think as much written texts as possible.

S2: 00:21:18.408 Yeah. Amen to all of that on my side.

S5: 00:21:21.706 Okay, then I think I understand what's expected of me, so I will remove myself from Minetest and stay on FLOSS history. And thank you.

S1: 00:21:42.631 FLOSS history could have basically text explaining what happened and interviews of people of that history explaining how they saw the event or saw the evolution of things.

S3: 00:21:57.905 Yeah, that sounds good.

S5: 00:22:01.982 Okay. And so, my question, then, would be, when should I start? With whom? Is it just me doing that, or is it me reaching out to Obergix, or is there getting together, start doing that? Is there an expectation of someone who will be [reviewing?]? That's actually very interesting for me, to be completely out of the loop, out of focus. And so I'm very sorry for that, but then I do have time. You just have to point me to something to do when.

S1: 00:22:48.799 Well, Obergix was not here last time either, so we appointed him because he kept saying he's an old-timer in free software world. So it seemed like a good place to put him.

S5: 00:23:01.209 Okay. So whenever I make progress on that, where should I write it down?

S1: 00:23:10.087 Oh, in merge request or an issue. If you come up with texts or like a--

S5: 00:23:18.444 The desired output, is text, right?

S2: 00:23:23.803 Yes, so far.

S1: 00:23:23.763 Yes, so far. So two types-- sorry.

S5: 00:23:26.987 So my next action is to create a merge request and then act on it, right?

S2: 00:23:37.411 Yep. If you can do a merge request on top of what I just copied, that would be perfect because then that will [inaudible].

S5: 00:23:44.556 On top of what you just copied where?

S2: 00:23:49.350 Yeah, the last merge request that I have in the chat, merge request 13. So that's the document I was mentioning, which has the structure from the Calc and the elements from the brainstorm. So these hopefully will be merged in because it's just a base structure, and you can create a pull request on top of that document.

S1: 00:24:12.372 And you can add text if you want. If you have a written text explaining the evolution of history or whatever, what you think will be seen by students, you can also make a merge request with this document, and you can also edit the brainstorm file to add questions that you would like to ask specific people, or any action item for the future you can also put in the GitLab.

S2: 00:24:48.228 You seem confused. Everything okay?

S5: 00:24:51.888 I think now, yes. I'm sorry--

S1: 00:24:56.245 Now, you're confused, or no?

S5: 00:24:58.266 No, I'm okay. I'm okay. The thing is what's interesting is I have some context. But since I'm very busy, I don't remember anything. So I jump in and I want to contribute. But of course, I'm a pain because you have to tell me where and when and what, which you just did. And I'm very grateful.

S2: 00:25:31.446 Yeah. And here it's not really that you're forgetting because a lot of those things are new. That's the first section. So that's the first document that we did, which I did only yesterday. So everyone is discovering what I did. So don't worry. We are all discovering all of that. And if you're not sure, there is always all the transcript by the way, even for your content, even though I've already integrated that part. You can always go back. And that's a way to go quickly through meetings without having to rewatch everything.

S1: 00:25:59.334 And if you want to say, "This is not an efficient way to progress, and I have better ideas," then feel free to suggest better ideas.

S5: 00:26:06.500 No, no. I'm a 101 contributor with no idea where to go, and you provided me the necessary items. So that's good. Okay.

S1: 00:26:21.674 And so the way to contribute with Obergix and the way to collaborate with him or to convince him to accept to do stuff is up to you.

S5: 00:26:37.517 Okay. I will do that right now.

S2: 00:26:42.062 And by the way, is he going to come back to join the meetings? Because I think you invited him originally, Marc. Do you want to just ping him on that?

S1: 00:26:50.162 I pinged him on Matrix like one hour ago, but I will probably ping him again.

S2: 00:26:59.466 That's probably best idea.

S1: 00:26:59.703 Since we are delaying the discussion about week two, we can ping him more [crosstalk]--

S2: 00:27:05.898 Yes because [inaudible] sounds good. And I don't know if Polyedre is [inaudible] meetings because I've seen him last week [inaudible]--

S5: 00:27:22.259 Is it just me or are you breaking up?

S1: 00:27:28.218 Sorry, Loic, just say we are losing you.

S5: 00:27:31.225 Yeah. You're breaking up. We heard 50% of the words.

S2: 00:27:40.299 Hello. Okay. So yeah, I was asking about Polyedre because I've not seen him last week or this week. I don't know if any of us has kept contact with him. Maybe we could just ping him on one of the [inaudible] on GitLab.

S1: 00:27:59.305 His perspective is that he's a student and wants to beta test or test stuff that we write, but I'm not sure. I understood that he was not specifically interested in writing contents. We lost Xavier, I think.

S3: 00:28:27.252 Technology is awesome.

S1: 00:28:31.890 Loic, you wanted to ask someone something?

S2: 00:28:36.648 Hello. Sorry. Yeah. Sorry about that. My connection is giving me trouble right now. Yeah. I was just - I don't know how much of that went through - asking about Polyedre because I have not seen him recently.

S1: 00:28:52.990 You did not get my answer, right?

S2: 00:28:54.918 No. I did not.

S1: 00:28:56.217 As far as I understood Polyedre is a student, and he wanted to beta test the MOOC, but he does not feel comfortable writing material for the MOOC itself. So I think it's fine to ping him to ask to proof check or to check that something can be understood, but I don't think we could ping him to write stuff.

S2: 00:29:27.456 Not necessarily to write stuff, but I was wondering if he even knows about the new set of meetings because I think he joined from the announcement of Framasoft, and only the first ones have been announced. So maybe I'll ping him on the brainstorming session ticket or something like that.

S1: 00:29:46.822 I saw him on GitLab, and I also saw him this weekend at a conference I was at. But we did not discuss the MOOC.

S2: 00:29:55.578 I'll still ping him just to make sure he knows about it then.

S1: 00:30:04.825 Loic, you wanted to ask something.

S5: 00:30:15.213 Yes. No, I wanted to share what I did. So I added that to my [inaudible]. So that's what I will do. That's my to-do, but it's just one item. But that's what I will do by next Saturday on the latest.

S1: 00:30:44.527 Yes. So the delay is a question I had. Do we delay every week, or do we delay week two to much later?

S2: 00:30:57.856 I think it would be good to push them back whole now. This way we'll continue with week two, otherwise, it's going to be [inaudible] I think. Sorry. That's more GitLab issues to edit though. [laughter]

S1: 00:31:14.829 That's life. I can do it.

S3: 00:31:21.194 I feel like we owe you something for dealing with all of this obnoxious admin stuff.

S2: 00:31:27.484 Maybe some Easter chocolate would be?

S1: 00:31:30.511 Yes, of course, chocolate. [laughter] Chocolate is always fine. I accept all chocolate.

S5: 00:31:37.969 Oh, a quick quiz. How many of you know about SoloKeys?

S1: 00:31:48.777 I know the name.

S5: 00:31:50.093 Do you know YubiKeys?

S1: 00:31:52.931 Yes.

S5: 00:31:54.130 Okay. Do you have one?

S1: 00:31:57.620 I have two.

S5: 00:32:00.440 Kendall, you don't have any?

S3: 00:32:04.188 I don't know what--

S5: 00:32:06.128 It's that two-factor authentication hardware token.

S3: 00:32:08.714 Oh. No, I don't have one currently.

S5: 00:32:11.258 Okay. And Xavier, you're not using any in your company, right?

S2: 00:32:17.266 Not in the company, but I've just ordered one from like [inaudible] and a free version of YubiKey basically on Kickstarter, so I've ordered both of those two.

S5: 00:32:29.077 SoloKey. It's SoloKey.

S2: 00:32:30.334 SoloKey, yes, exactly.

S5: 00:32:31.304 And wait, it goes back to what we owe to Marc, and last week I figured that giving a SoloKey or a kind of two-factor authentication key to a geek, or someone who is somewhat geeky is actually nice gift, and not too expensive. But that was--

S2: 00:32:59.031 It's true because strangely those are [inaudible].

S5: 00:33:06.435 You're breaking up.

S2: 00:33:08.105 Yeah.

S1: 00:33:08.969 Maybe we can get the webcams. Or, Xavier, maybe you can get your webcam if that helps with your connection.

S3: 00:33:21.258 Oh, no.

S1: 00:33:22.429 Oh, he will come back. [laughter] I will put a lower quality.

S3: 00:33:30.368 Quick, assign them all the things. [laughter]
[silence]

S1: 00:33:45.308 He's coming back.

S3: 00:33:47.026 Yeah.
[silence]

S5: 00:34:31.336 Kendall, maybe it's a question for you. In your development of the Upstreaming course for OpenStack, do you reuse some content that you found online with a proper license? That's something I never did but seems to make sense, instead of making content trying to harvest whatever content we can find out there.

S3: 00:34:58.142 Since most of it is specific to OpenStack and the tools that we use as a community, so Zuul is like our CI/CD system. Since all of that is community run and organized and whatnot, we haven't pulled things from external sources just because everything is pretty specific.

S5: 00:35:23.879 Yeah, that makes sense.

S3: 00:35:25.066 But yeah, it's the thing that we definitely pay attention to in terms of crediting the right places when external resources are used.

S5: 00:35:35.271 Yup, I was reading what was written for the FLOSS history part, and it's a story that has been told over and over again by some very good people, so that's a place where reusing videos and contents would be [crosstalk].

S1: 00:36:00.937 It does make sense.

S3: 00:36:03.905 Yeah, I think making references to external sources is fine too so long as it's credited.

S5: 00:36:12.726 Oh, yeah, you mean when it does not have the proper license to be included? Yeah, correct?

S3: 00:36:19.368 Yeah, yeah.

S5: 00:36:21.296 That's a nice idea.

S3: 00:36:23.564 Yeah. Well, we had talked about making this meeting a little bit shorter. So in the interest of trying to push that forward, were there other things that we needed to talk about updates on, or anything? I know Remi is not here. He has a lot of these work items.

S1: 00:36:46.443 Yeah, and then there is Xavier for the fifth item?

S3: 00:36:52.270 Oh, yeah.

S1: 00:36:53.290 Xavier is back, right?

S2: 00:36:55.299 Yeah, and I switched my connection. Hopefully that will hold better. Let me know if not. So yeah, for that one I also created yet another pull request. So I'm trying to find that. Yeah, project tips. Here we go.

S3: 00:37:13.644 Just showing everybody up by how much progress you made?

S2: 00:37:16.649 Oh, yes. But actually a day fully dedicated to one thing, like my whole Sunday was just the Upstreaming course: no interruptions, no emails, no meetings. It's crazy how much more productive we are [inaudible] right? So yeah, that's the ones who say-- I also based it on some of the brainstorm, but I have developed a bit the content. So reviews are also very welcome. I see that Jill and Farhaan from OpenCraft have already done a [post?] today, which is great. So yeah, I've tried to apply some of my best practice, some of the stuff we discussed, but obviously, it would be good to improve. It's not like we did the finalized one.

S3: 00:38:05.355 Yeah.

S1: 00:38:06.662 Well, lots of stuff to read.

S3: 00:38:10.448 Yeah, I hope it doesn't disturb my whole-- I will make sure to do a review before next Monday.

S2: 00:38:18.188 Cool, thank you.

S1: 00:38:18.526 I will probably review it tomorrow if I have less [center?] meetings tomorrow.

S3: 00:38:27.459 Or if something else really important pops up that you're trying to procrastinate on, it's a good time.

S1: 00:38:34.148 I have only two meetings tomorrow, so I'd probably have the time to read and comment on that.

S3: 00:38:41.646 Cool. So I think that covers just about everybody and their to-dos from last week. Right?

S2: 00:38:51.726 Yep. And actually, just for that review, Loic also, if you could have-- if you have some time obviously because I know that you have already your items, but given that's also something that you thought a lot about especially from the mentoring side and figuring out what the best first steps are and etc., [that would be useful?].

S5: 00:39:13.294 I don't think I will.

S2: 00:39:16.383 Okay, well at least that's--

S5: 00:39:16.680 Just so you don't have your hopes up. But I have in mind that the mentoring part is still an open problem. But yeah.

S2: 00:39:29.755 Yep. This one is not really about mentoring per se, but it's more about giving activities to people to start doing.

S5: 00:39:36.849 Yeah, but I did take a quick look. So it's tangent to the [inaudible].

S2: 00:39:41.547 It's tangent, yeah.

S1: 00:39:49.242 Okay. If anyone has any points they want to discuss?

S5: 00:39:54.253 I have just one question. If I want to ping Obergix, there is the Matrix chatroom and this project on GitLab. No other channel, right?

S1: 00:40:10.353 Email.

S5: 00:40:12.776 Correct?

S1: 00:40:17.004 I think we have his email, olivier.berger@telecom-sudparis.

S5: 00:40:21.787 I know. Just I want to keep that publicly archived in writing.

S1: 00:40:27.865 Oh, yeah. In public, he has his Matrix and his--

S5: 00:40:33.120 This project on GitLab? And that's the two communications channels that we know and we use. Okay. Excellent.

S2: 00:40:42.650 I don't know if we have his GitLab handle to ping him.

S1: 00:40:47.859 Probably Obergix. I'm almost sure it's Obergix.

S2: 00:40:52.741 Okay.

S1: 00:41:02.290 I will check, but.

S2: 00:41:10.300 All right. So anything else for today?

S3: 00:41:19.869 I don't think so unless somebody wants to volunteer to steer the next meeting, but that can also--

S2: 00:41:27.226 Oh, yeah. It's true. It's true. We talked about assigning the different sections, right?

S3: 00:41:33.744 Yeah.

S2: 00:41:34.874 It's true that maybe we could have a quick look at that.

S3: 00:41:40.993 I would really just do it one week out at a time. We don't need to plan super far into the future. So if we know who wants to do the next one, which looks like it's on-- what's the second week?

S2: 00:41:54.992 It's on Git and pull request and stuff. Well, on my side, looking at the list of weeks, I know that it's probably the first week that I'm the most interested. However, that's already something that you're handling, Marc. If you want to exchange, I will take it. Otherwise, potentially the second week. But on my side, I would prefer the first week.

S1: 00:42:20.944 Okay. I'm trying to find the right tab. I have 1,200 Firefox tabs, and I'm always losing the ones I'm looking for.

S3: 00:42:36.543 Oh, god. Are you going to keep using that same spreadsheet for everything, right? The Framacalc?

S1: 00:42:49.000 Yup.

S3: 00:42:50.138 Cool. All right. I will make sure to save that. Yeah. Cool. Now we're good to go. You volunteering to contribute one other week?

S1: 00:43:04.196 I'm trying to see if I want to. If I'm much more interested in other weeks, then I can leave the first week to Xavier. I will probably--

S2: 00:43:14.540 Sorry. Looking at the list, if you want, I could take week four also because this one is really about contributing and community management, which are also things that I like. And I guess in the first week, that's the part that I like is getting people in touch with free software and starting to contribute some things. So yeah. Either way could work fine, but I could just take week four right now.

S1: 00:43:43.200 Yeah. I have many weeks that interest me, also, especially week three and week probably five and six. So yeah, I can leave you the week one.

S2: 00:43:57.727 Okay. Sounds good. I'll take that for now. And if it's done by then or if it's moved forward, then I'll look at week four. But there will be some time before that anyway.

S1: 00:44:07.369 Yep.

S2: 00:44:08.891 Okay.

S1: 00:44:09.618 Okay. So I have an action item to delay every meeting by a week in the issues.

S2: 00:44:17.316 All right.

S1: 00:44:19.047 And yep, thank you, everyone, for attending, especially since it's a holiday.

S2: 00:44:26.428 Yes, definitely.

S1: 00:44:27.497 So thanks a lot for everyone for--

S5: 00:44:29.674 And thank you for making it easy for me, a contributor, [a little bit?] more time. Perfect. I reached out to Obergix on Matrix, and so that's--

S1: 00:44:40.946 And thanks, Xavier, for involving the other OpenCraft people because I saw comments they put.

S2: 00:44:51.592 Yeah. They are really interested in that thing, so they were quite happy to get involved in this. So yeah. All right. Well, see you soon.

S5: 00:44:59.090 Great. Have a nice day. Bye.

S2: 00:45:00.388 See you next week.

S1: 00:45:01.595 See you next week.

S3: 00:45:02.205 Bye. See you. See you next week. Bye.

S6: 00:45:08.434 You are.
