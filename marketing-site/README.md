# MOOC FLOSS Marketing Site

## Installation

This project uses [SvelteKit][svelte-kit] to generate a static site for the MOOC
FLOSS course.

Install dependencies from the marketing site folder using NPM:

```shell
$ npm install
```

## Development

For convenience, there's a `.tmuxinator.yml.example` file that can be used to
quickly bring up a full dev environment with a single command. Install tmuxinator,
copy the example config, and then run tmuxinator in the root of this project:

```shell
$ brew install tmuxinator

$ cp .tmuxinator.yml{.example,}

$ tmuxinator
```

Or, you can run the project manually:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

### Changelog-friendly commit messages

For creating changelog-friendly commits, you can use `npm run commit` once
changes are staged. This will use [commitizen][commitizen] to prompt you for
information about your commits.

### SVGs

Different vector creation tools generate vectors differently. To account for
this, SVGs that are imported using `sveltekit-svg` have a few transforms applied
to them (see [`svelte.config.js`](./svelte.config.js)):

- IDs are removed from `svg` elements to fix accessibility issues with multiple
  IDs with the same value rendered in the document
- `width` attributes are added to `svg` elements to ensure SVGs are by default
  rendered at their native dimensions

## Building

```bash
npm run build
```

### Sass and PostCSS

This projects uses Sass to build styles, and uses PostCSS for the following
features:

- support for legacy browsers via `autoprefixer`
- support for CSS logical properties via `postcss-logical`
- support for CSS custom properties via `postcss-custom-properties`

### SVG Sprite

An SVG Sprite is built into `./src/assets/img/sprite/build` before building and
during development when files in [./src/assets/img/svg/sprite](./src/assets/img/svg/sprite)
change. See [package.json][package-json] for svg scripts.

## CI / CD

Changes to any files in `./marketing-site` will trigger a pipeline in GitLab.
Previews of the marketing site are built for every pipeline, and are accessible via
[https://mooc-floss.gitlab.io/mooc-floss/:pipeline_id]. Please see the pipeline
logs for output.

The following files are used for builds and deployments:

- [../.gitlab-ci.yml](../.gitlab-ci.yml)
  - linting and link-checking
- [../.gitlab/scripts/build-marketing-site.sh](../.gitlab/scripts/build-marketing-site.sh):
  - building the marketing site
  - copying the marketing site to a public folder for GitLab pages to serve
  - compressing assets using `gzip` and `brotli`

<!-- LINKS -->

[svelte-kit]: https://kit.svelte.dev/ 'Svelte-kit'
[commitizen]: https://www.npmjs.com/package/commitizen 'Commitizen'
[package-json]: ./package.json 'package.json'
