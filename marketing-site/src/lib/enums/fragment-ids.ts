export enum FragmentId {
  Course = 'course',
  WhoIsItFor = 'who-is-it-for',
  WhyFloss = 'why-floss',
  WhoWeAre = 'who-we-are',
  Subscribe = 'subscribe',
}
