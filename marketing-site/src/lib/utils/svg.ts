export interface PathData {
  length: number;
  path: SVGPathElement;
}

function preparePathsForAnimation(svg?: SVGElement): PathData[] {
  const pathsData = svg
    ? [].slice.call(svg.querySelectorAll('path,line')).map((path: SVGPathElement) => {
        const length = path.getTotalLength();

        path.style.strokeDasharray = `${length}px`;
        path.style.setProperty('--stroke-dash-offset', `${length}px`);

        return {path, length};
      })
    : [];

  return pathsData;
}

export {preparePathsForAnimation};
