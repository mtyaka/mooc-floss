export enum IconId {
  Burger = 'burger',
  ChevronLeft = 'chevron-left',
  ChevronRight = 'chevron-right',
  Tick = 'tick',
}
