import AnimatedSvg from './animated-svg.svelte';
import AnimatedSvgText from './animated-svg-text.svelte';

export {AnimatedSvgText, AnimatedSvg};
