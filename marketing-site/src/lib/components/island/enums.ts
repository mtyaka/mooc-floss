export enum IslandModifier {
  Base = '',
  Start = 'start',
  End = 'end',

  SmallerStart = 'smaller--start',
  SmallerEnd = 'smaller--end',
  SmallerStartDesk = 'smaller--start--desk',
  SmallerStartPalm = 'smaller--start--palm',

  Smallest = 'smallest',
  SmallestPalm = 'smallest--palm',
  SmallestStart = 'smallest--start',
  SmallestStartPalm = 'smallest--start--palm',
  SmallestEnd = 'smallest--end',
}
