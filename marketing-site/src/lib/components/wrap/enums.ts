export enum WrapModifier {
  Base = '',
  Large = 'large',
  Larger = 'larger',
  Small = 'small',
  Smaller = 'smaller',
}
