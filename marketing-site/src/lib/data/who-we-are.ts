import {urls} from './urls';
import {partners} from './partners';

const whoWeAre = [partners.framasoft, partners.openCraft, partners.openStack, partners.imt];

const ourSupporters = [
  partners.imt,
  partners.openCraft,
  partners.openEdx,
  partners.openStack,
  partners.telecomParis,
  partners.patrickAndLinaDrahiFoundation,
];

const contributors = [
  {
    name: 'Marc Jeanmougin',
    url: 'https://marc.jeanmougin.fr/',
    bio: `Research engineer at
      <a href="${partners.telecomParis.url}">
        ${partners.telecomParis.name}
      </a>
      and a contributor to free software such as
      <a href="${urls.inkscape}">
        Inkscape
      </a>
    `,
    image: 'marc-jeanmougin.jpg',
  },
  {
    name: 'Rémi Sharrock',
    url: 'https://remisharrock.fr/',
    bio: `Researcher and Lecturer (Associate Professor) at
      <a href="${partners.telecomParis.url}">
        ${partners.telecomParis.name}
      </a>
    `,
    image: 'remi-sharrock.jpg',
  },
  {
    name: 'Xavier Antoviaque',
    url: 'https://www.linkedin.com/in/antoviaque/?originalSubdomain=fr',
    bio: `Founder and CEO at
      <a href="${partners.openCraft.url}">
        ${partners.openCraft.name}
      </a>
    `,
    image: 'xavier-antoviaque.jpg',
  },
  {
    name: 'Geoffrey L',
    url: 'https://www.linkedin.com/in/hfd8f83423j823hd2328d32h32d239d238d3/?originalSubdomain=jp',
    bio: `Fullstack software engineer at
      <a href="${partners.openCraft.url}">
        ${partners.openCraft.name}
      </a>
    `,
    image: 'geoffrey-l.jpg',
  },
  {
    name: 'Jillian Vogel',
    url: 'https://www.linkedin.com/in/jillianvogel/?originalSubdomain=au',
    bio: `Senior open source developer at
      <a href="${partners.openCraft.url}">
        ${partners.openCraft.name}
      </a>
    `,
    image: 'jillian-vogel.jpg',
  },
  {
    name: 'Olivier Berger',
    url: 'https://www-public.imtbs-tsp.eu/~berger_o/',
    bio: `Research engineer at
      <a href="${partners.imt.url}">IMT</a>
    `,
    image: 'olivier-berger.jpg',
  },
  {
    name: 'Anna Khazina',
    url: 'https://www.linkedin.com/in/anna-khazina/?originalSubdomain=fr',
    bio: `Digital learning project manager at
      <a href="${partners.imt.url}">IMT</a>
    `,
    image: 'anna-khazina.jpg',
  },
  {
    name: 'Loïc Dachary',
    url: 'https://blog.dachary.org/',
    bio: `Free software developer`,
    image: 'loic-dachary.jpg',
  },
  {
    name: 'Polyèdre',
    url: 'https://gitlab.com/polyedre',
    bio: `Computer science student`,
    image: 'polyedre.jpg',
  },

  {
    name: 'Kendall Nelson',
    url: 'https://twitter.com/knelson92',
    bio: `Upstream developer advocate at
      <a href="${partners.openInfrastructureFoundation.url}">
        ${partners.openInfrastructureFoundation.name}
      </a>`,
    image: 'kendall-nelson.jpg',
  },

  {
    name: 'Ildiko Vancsa',
    url: 'https://www.linkedin.com/in/ildiko-vancsa-a963499/',
    bio: `Senior manager, community and ecosystem at
      <a href="${partners.openInfrastructureFoundation.url}">
      ${partners.openInfrastructureFoundation.name}
      </a>`,
    image: 'ildiko-vancsa.jpg',
  },
];

export {whoWeAre, ourSupporters, contributors};
