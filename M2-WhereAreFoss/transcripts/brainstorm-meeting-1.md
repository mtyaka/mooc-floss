# Brainstorm - Module 2 (Meeting Transcript)

Video: https://digimedia1.r2.enst.fr/playback/presentation/2.0/playback.html?meetingId=73d3cd53ec8ede3f636b9a782ecc9f6375cf2271-1613410202827

Rémi: 00:00:00.000 [Et toi?].

Marc: 00:00:02.461 Okay. So thank you everyone for coming here to this third brainstorming session about second week. So this week will be module two so that students will learn about Git, GitLab, and GitHub because these are the main platforms around which there is collaboration on the FLOSS software. There are some others like Launchpad, like a Redmine, or Mercurial, but most FLOSS projects now have GitHub or GitLab presence, so I think it's safe to just teach Git and tell them to adapt if it's not Git. So I'll make a quick introduction. So I'm Marc Jeanmougin. I'm an Inkscape contributor. I'm a research engineer at Télécom Paris in France. So Rémi?

Rémi: 00:01:10.389 Rémi Sharrock here. Assistant professor. Researcher at Télécom Paris also in the computer science department. Olivier?

Olivier: 00:01:21.886 Yes. Hi. I'm Olivier Berger. I'm a research engineer in Télécoms Paris, which is the younger sister of Télécom Paris. I'm also in Paris region and I've been involved in free software since '96, I guess. So yeah. I'm working also at the moment on setting up another MOOC that is funded by the same institution, [inaudible] in Télécom, which is about Git, so quite to the point.

Rémi: 00:02:02.834 Yes. Xavier?

Xavier: 00:02:07.105 Yep. So I'm Xavier Antoviaque. I'm from OpenCraft. We are a provider and contributor on the Open edX project. And yeah, I've also been with open source and free software for quite a long time. I think 20, 25 years by now. And yeah, I'm also interested in this MOOC because I think that's currently something really missing, having a good way to onboard people to contribution and app streaming.

Rémi: 00:02:37.230 And finally polyedre.

Polyedre: 00:02:39.627 Hi, I'm polyedre and I'm a computer science student, and I'm here to beta test the MOOC and to give what I want as a student, what I want to learn.

Rémi: 00:02:53.433 Very good. Thank you very much. So that was introductions. And the second point in the agenda is, "What should be the scope of this week?" Or we could talk about modules instead of week because week is more timely, I would say, and modules, you can do one module in the period of time that you want. Anyway, so the second module is about Git, GitHub or GitLab, merge request, bug management, etc. And the two brainstorming sessions we had before were about, of course, the first module, 'What is free software?' and a global introduction. And the very first brainstorming was about the whole structure of the course. And we already had very good contributions and many ideas. So I hope it will be the same for today's session. So as a very brief introduction of the contents, we have four categories. So it's the different columns you can see here. One is about transmission, so everything you can read, listen to. It's not really an activity. It's only an activity of reading or listening to something. So that is called transmission. Another type of contents we have is interaction when we ask students to talk to each other or to talk to other people outside of the MOOC, or interaction between one mentor and one student, or one teacher and one student, or I don't know. Activities are the third type of contents. And it's more about doing something in lab or researching something on the web and doing a synthesis. I don't know. Any kind of other activities that involve something more than listening or reading. And these activities are not part of the certification at the end. So it means that you don't have any grade. Whereas if we compare to evaluation, these are also activities, but evaluated with a grade at the end. And the type of grading will depend on the whole certificate. So you will have to get the correct grade so that you have the certificate at the end. And that's it. So these are the four types of contents we will be discussing about, and specifically about how to-- not teach, but how to know more about Git, GitHub, GitLab, why it is important for contributing to FLOSS projects and what we should do as an activity, how we should interact, and how could we evaluate for the certification what has been done during this module. All right. So the first question, I think, that is important is, we should define some prerequisites for this module.

Rémi: 00:06:35.237 So we were talking of-- and so before the start of this brainstorming, we were talking about the command-line interface. So indeed, we were asking if basic command-line interface commands, I mean, are required to do this MOOC or should we give some basic content about, "What is it?", how we should use the command line. And the question behind is we would like to use Git, of course, but should we focus more on Git on the command line or also Git all integrated already in integrated development environments like VS Code or any other kind of environment that maybe doesn't need to know how to use Git in the command line. So these are the prerequisite of this MOOC and also the prerequisite of this week. I think this is a very important question to ask. And also, if you have any ideas on tools to learn Git maybe quickly or to learn how to use the platform, GitLab, GitHub. Or any kind of idea you would have is welcome.

Xavier: 00:08:10.524 Well, we tend to think that the less requirement we have, the more open the course is going to be. So you see, if you don't know command line and you try to do some contribution, that might be a bit tricky at times. But who knows? After all, we were even talking about the fact that, potentially, we would like to have non-quick contribution maybe one day or something like that. So I think it can make sense to try to keep it minimal. And of course, I would like maybe sections that might be more optional that require the command line. But if some people don't have it, don't want to learn it now, it could also make sense, yeah, to have either the part that's integrated into the IDE or even main pages like GitLab interface for fixing a typo or even a very small bug. That might be enough at that point. I think that the core is not really technical in this MOOC at least or the way is it.

Rémi: 00:09:16.220 So Marc, did you want to open the MOOC to people that want to contribute to FLOSS projects but without any knowledge of coding, or is that something that we should [crosstalk]?

Marc: 00:09:35.517 In my opinion, this MOOC was targeted as a FLOSS contribution for code contributions so that it does not have to deal with all the diversity of other possible contributions and how they interact with each other in detail because if we market it like FLOSS contributions in general, then we might have people who would think it could be basically, how too do a contribute design? How do I contribute-- I don't know, other stuff. And for which the MOOC might not be adapted. So in my opinion, it would be better to focus on code contributions. It does not directly imply command line because it's technically possible to code without using the command line, more in some programming language than in others. Perhaps Eclipse users see less command lines than people using REST or I don't know what.

Rémi: 00:10:59.426 JavaScript also, maybe.

Marc: 00:11:00.785 Yeah. Maybe. Nope. I don't know. And it's technically completely possible to use Git without using it in command line, but I'm wondering if it's as easy to evaluate Git knowledge without having to resort to some form of command line. Because basically, if Git without command line is Git in an IDE or in some software like Precise Software, and we could have a web UI for GitRepo or something like that, but I didn't search for any, so I don't know any. But it might be harder than using stuff like JSLinux or WebLinux or basically anything command line because the command-line interface of Git is always the same. So it--

Xavier: 00:12:16.866 Sorry. Go ahead.

Marc: 00:12:18.483 So it might be harder to adapt knowledge that you get by using some UI interface for Git and to adapt what you know about Git as basic commands, which are always implemented in some form of UI. So--

Olivier: 00:12:41.164 May I--

Xavier: 00:12:43.277 Sorry, go ahead. Sorry [inaudible].

Olivier: 00:12:44.524 --just give a few hints? First of all, as I mentioned in my short introduction, we are currently working on trying to create a MOOC on Git specifically. So that could be some kind of remediation in case people lack some knowledge. It could be a prerequisite up to some extent, provided that we are ready. But I guess you are more advanced than we are. So I don't know what was the deal. Well, how soon we are forced to deliver and what else, but. So that's the first point. Then, I think it's quite important to consider that for many users now, the situation is that the web interfaces of GitLab or GitHub provide quite a lot of tools or a web interface of some kind whereby I was mentioning earlier before the start of this recording that we are working on another project on Eclipse Che, which is a kind of web IDE. So if you go to GitLab, for instance, you can see that for pretty much any repo they now provide something called the Gitpod that can start a development environment in the web interface, which provides some kind of a command line if you need it somewhere. You have this--

Rémi: 00:14:21.807 Is it web IDE or something else?

Olivier: 00:14:24.894 Yes. So it is called Gitpod, and a few months ago, not that long ago, GitLab and Gitpod announced they would be providing Gitpod as a web interface or web IDE for GitLab and some else. So basically in GitLab, you have a button that you can click which brings you to an alternative view which provides you with a checked out copy of the repository, if you all know what I mean, on which you can hack. So it's more than a web editor. It's a whole IDE. And if you provided some kind of Yammer file in the repo, it can instantiate a special view with tools and so on. So basically, you have containers to pass continuous integration, tests, and so on. Stop me if that's too much of details. But yeah, the idea is clear?

Marc: 00:15:19.679 No, it's okay. [crosstalk].

Rémi: 00:15:23.033 Olivier, indeed in GitLab, just next to the web IDE button, when you are in Git repository, you can choose between the old, I would say, web IDE, quickly and easily edit multiple files in your project, and Gitpod indeed.

Olivier: 00:15:45.413 Yeah, that's it. That's the one I'm talking about. So it means that people have realized, I guess, if I can generalize, that probably the set up where you have your own laptop and you would give clone, and you would make an edit, and you would push, and you would ask for repo request or a merge request, depending if you are on GitHub or GitLab, is something of the past. Now, the idea is that you have a cloud where you can only use the web interface as a tool that you need to install and you need to get access to make more advanced stuff than what you could do on your laptop if you had to install a compiler and so on. So that's the cloud, the software as a service variant of the development environment. Of course, this is questionable in many sense and there is a question of cost of such environments because if it runs on a cloud, it needs to be funded somehow, and so on. So I guess for most projects, if you want contributions that go beyond a single line edit or stuff like that, this can provide great value, which means that maybe you don't need to get used to the command line having either the PowerShell or Bash locally, and on.

Xavier: 00:17:24.988 Damn, this is great, actually. Thanks for noticing this because I was using the GitLab way or web IDE, but this looks a lot nicer.

Olivier: 00:17:34.593 Yeah. It's like you put your development environment on the cloud. And what's interesting here is that you can have contributors which are using Linux, Mac, Windows, whatever, or just a Chromebook. Maybe a tablet, but a tablet you need some kind of a keyboard somehow, probably. And then you can code from pretty much anywhere, and the working environment, all the tools you need are already provided, already installed, already customized. So there is no surprise that the barrier to entry is not, "Okay, stop buying single compiler, but not that version. Oh, and you did this library but not that one," and so on. Okay.

Xavier: 00:18:16.851 That's pretty great, actually. I mean, I'm guessing there is a little bit of work on the side of the project to make sure that the development environment is actually ready to be used there, probably, because when I think about Open edX, which use VMs and dockers and whatever to have the development environment, there is probably a step there. But still, that's a huge step at the beginning. So if that can be automated, that's all great. And I think--

Olivier: 00:18:42.616 In principle, all that relies on Kubernetes as a [reddening?] environment which hosts containers and stuff. So the web IDE is in a container, the compiler is in a container, the test environment is in container, and the web application that you are trying to hack on, that you are contributing to can be started in a container on its database with the proper test data and so on. So everything is in the context of the DevOps principles and so on. And it means that it's quite a platform as a service experience compared to traditional development environment on your local machine. I don't know if that makes sense to you, depending on your context and so on. But in the context of Open edX, for instance, the difficulty might be to deploy a web application and a database and so on, and all the prerequisite may be held up and so on. But all those tools are already available in containers. You just have to click and that starts magically. In principle. If it works.

Rémi: 00:19:53.929 So GitHub is doing the same. So it's really a tendance. I don't how to say that in English, but.

Olivier: 00:20:01.542 The trend.

Rémi: 00:20:03.145 Yeah. GitHub has the same feature that is called Codespace. So it is basically a Visual Studio Code. I think it is the same as Gitpod. Gitpod is using Visual Studio Code interface as well, right?

Olivier: 00:20:19.369 Well, it's Eclipse Che but it's more or less the same because it's merging now.

Rémi: 00:20:23.177 [Fair enough?].

Olivier: 00:20:24.878 Yeah. Eclipse Che, yeah. Yeah, Eclipse Che. Yeah. Probably.

Rémi: 00:20:27.886 Okay. So it's the open source version of-- open source web version of Visual Studio Code indeed. More or less. Without proprietary extensions and stuff. All right. Yeah.

Olivier: 00:20:46.854 So back to the question of maybe the need to do the command line, I think it could be interesting. But more important is the concepts like what is a tree of versions? What is a merge? What is a merge request? And all that. And that there is no single copy. That you can have multiple copies and each developer can have his clone and so on. So navigating in this archaeology of forks and stuff is probably more important, in my opinion, than just being proficient in command line because the command line is awful. If you don't practice every day, it's a pain. I mean, so.

Rémi: 00:21:36.584 Very interesting. I remember last brainstorming, we were talking about the fork concept that is indeed quite central to contributing to FLOSS in general, and.

Olivier: 00:21:55.926 And it's funny to see that clone, fork, and [inaudible] may not be called the same way in different platforms. I'm not sure what I am saying, but it seems to me that at least the pull request and merge request are different between GitHub and GitLab and [crosstalk]--

Xavier: 00:22:14.710 So name are different but it's the same thing?

Olivier: 00:22:16.697 Yeah. Yeah. But the fact that they named it differently might just be a question of trademark or something like that or more profoundly, some kind of semantic notion that they wanted to present differently to their users. So I think that it might be important that just those questions about vocabularies that are different between platforms and so on are important to-- yeah, to explain in concept and just the fact that it's not named the same but it's the same might be confusing to some newcomers or some potential contributors. So maybe there is something beyond command line and so on. More the concepts, and the notions, and.

Rémi: 00:23:10.997 So maybe we should start-- I mean, I know that Xavier, you like the pedagogy that is more do before understanding something. Do something. Could you think that we start with an activity?

Xavier: 00:23:37.906 Yeah. I mean, I think it's always good to have both at the same time, but yeah, I agree that in general, that the earlier you start to actually do something, the more at least [prominent?] you tend to remember, especially for something which is like upstreaming. So it's [inaudible] action rather than just consuming things. So that makes sense. And I'm guessing there are things we could do because even in our environment, there could always be just a separate GitLab repository, which doesn't contain necessarily anything really interesting, but where people would have to create a merge request, have a one-commit merge, or something like that. And then that could probably be in large part automated because if we know the GitLab username of the person, then we can detect when they have created a merge request merged automatically, potentially this kind of thing. So yeah, it might not need to be a sandbox environment. That's what I meant there.

Marc: 00:24:42.469 I totally agree. We are a bit early for the technical part of how to evaluate or how to make something interactive, but my idea for that would be to have a hosted GitLab instance with an LTI integration because GitLab uses something called OmniAuth for authentication, and I saw a project to make an LTI OmniAuth bridge or something like that. So it could be possible to authenticate students with LTI on GitLab, which would be a great thing.

Rémi: 00:25:33.589 There has been a lot of [inaudible] because [inaudible] has hosted many MOOCs that were connected to [inaudible], so there were some kind of indirection but there were also a few pending open issues, I guess, about LTI, that may not be maintain or maybe neglected, or that they did the work but they didn't notice the issues. I don't know. And in any case, that's what we plan to do for the Git MOOC. To have GitLab somehow hosted most likely, but. So I guess we should work together because we have probably the same ideas. Same means. Yes.

Marc: 00:26:31.546 So Xavier, what do you think about the feasibility of this?

Xavier: 00:26:38.675 Of which one? Having the dedicated GitLab instance?

Marc: 00:26:44.324 No, having a dedicated GitLab instance is easy, I think, but basically, automatically having an account from your Open edX account and being able to detect if an account on GitLab did, for instance, a merge request with a passing pipeline [inaudible], or something like that.

Xavier: 00:27:13.094 Well, there might be ways. You would probably need to set up an external operator for this, which maybe is through LTI. There might be a way to link it with SSO in some way, this [way?]. We need to check exactly because I don't know exactly how far we can go with the SSO on GitLab directly, seamlessly, but I think at the minimum, we could always have just a field that asks for the GitLab username in the worst case. And probably we can have something that's better integrated than this. So yeah. We tend to think that something is doable with that for sure. I just don't know how to [inaudible]. How seamless it can be. But that's definitely something we could do a quick discovery on when we start looking at implementing that, and maybe having the first version, which is very minimal with languages like the minimum level of code or integration, and just improve on that.

Rémi: 00:28:28.407 Beyond the scope of that particular MOOC, I guess we can all be quite interested in LTI integrations of GitLab provided that the standard is standard enough for usability, but I guess would be interesting having GitLab and model, for instance, in another context at my university. So anyway, if it's more or less the same as the Open edX. But I am doubtful because LTI seems to be a standard that like all standards may have variations. But I'm not [crosstalk].

Marc: 00:29:05.808 Yeah. Generally, LTI stuff that work in models, you need to check and alter a few things in Open edX and the other way around because you have many different optional and subparts of the standards and depending on how much of that is implemented. But yeah, it's a good remark that this might be useful beyond this MOOC. So it might be good actually, setting up a separate repo project for it or something like that.

Rémi: 00:29:32.916 While we were discussing this idea for GitLab integrated as an AGI stuff, I was a copy pasting in the chat two links. One is [inaudible]. I don't know if you have already played with this website. It's quite popular, I guess, but there are many, many stuff around Git. So that one is graphical and can be interesting because it gives you a kind of sense of the command line and having a visual feedback of what you're doing. So would be helpful, maybe. And another one which is more disruptive, which is Oh My Git!, which is just one of the projects named that way, which is a game. It is currently developed but yeah, very much-- oh, sorry. It is in the works at the moment.

Xavier: 00:30:32.919 It's experimental.

Olivier: 00:30:34.782 No. No. Well, it's [crosstalk].

Marc: 00:30:36.623 I did click and it did not seem finished.

Olivier: 00:30:39.578 Yeah. It's moving fast. [laughter] And so there are two developers paid full time I guess to work on that project that were sponsored by a German funding project for common goods. And so their aim is to have a card deck game for practicing or learning Git. So it's quite exciting, but maybe depends on what you're testing because it's moving fast and breaking a lot. So it's based on the Godot game engine so it's for open source and so on. And I think it's quite a nice idea because of the gamification that it provides. But maybe the [inaudible] approach is weird and so I haven't tested enough to figure out. But I think it could be something interesting also to have an eye on.

Rémi: 00:31:44.118 And it is using the command line, of course.

Olivier: 00:31:48.973 It's a mix. You can use the command line and it uses Git and [NF?], so it's interesting because it's based on Git. It's not a simulator. But it also uses cards that you can use in a game where you trigger spells, you cast spells or stuff like that. So we have to test it in a few weeks when it's done or when the major [inaudible] will be done.

Marc: 00:32:22.852 Oh, it's just in a few weeks?

Olivier: 00:32:25.546 I guess it was the end of February and they just made a presentation at the first [inaudible]. And I'm waiting for the recordings to be available to figure out because as it's moving fast, what they aim to do and what they are doing-- what you can test is probably different. So I'm not sure exactly.

Rémi: 00:32:48.240 And so the question is also, how much of Git and how much of the platform for ActivityPub protocol? So how do you do a merge request on the platform? How do you discuss on the-- how do you use the platform globally? So both are very important to contribute to FLOSS because you have to know a little bit about Git but also how to use the platform to discuss with the community and do a merge request at the end.

Olivier: 00:33:33.911 There are also issues of workflows that you have to follow for branches and stuff. You have sometimes some code reviews process in the issues or in the merge requests, but.

Rémi: 00:33:52.128 Indeed some of the projects have a lot of quality verification pass automatic, so we should talk about this also.

Olivier: 00:34:07.224 It depends how far you may want to go or you want to.

Rémi: 00:34:12.857 But at least they have to know that this kind of process exists. Sometimes just a few words.

Olivier: 00:34:23.130 When I was thinking about-- excuse me, Xavier. When you're thinking about what changed when GitHub appeared and a pull request appeared? Basically, there was a, I think, there was an explosion. It made a change, which is an order of magnitude higher than what existed before because basically, there is no barrier to forking and there is a mechanism that is rather easy to manage to keep some kind of [inaudible] to avoid it to explode in something that is uncontrollable, that is manageable, where everyone will make her own fork and you know, [inaudible] know what is the conversion. So it's kind of the forces that are going outwards or inwards-- no, what is the English for--? When you turn and you have to ride your bike and not to fall off the road, you have a balance of two forces. And then there are mechanisms in the projects to make sure that you can [attach?] contributors and no one will fork too much. And that's important to present those two mechanisms. There you have the liberty to fork if you need. You can do whatever, but then you have mechanisms to make sure that the community will go on. But how far should we go in describing all those? Because there are so many different projects with so many strategies, so.

Marc: 00:36:15.344 But also fork in the context of GitLab and GitHub is something that's necessary, trivial, and encouraged, which is different from the fork project with its community. Having a local copy is called fork while it's just a cloud clone.

Olivier: 00:36:41.057 Yes, but if you have projects with well-established procedures and communities and discussion venues where people can coordinate in general, it's rather easy to figure out what is the main repo and avoid proliferation of forks like uncontrolled clones that you don't know who's making what. And the problem arises when there is no maintainer and there are like 15 copies with a patch here and there because those projects were abandoned where and people just forked for and never tried to contribute and then never learn about making an issue, making up a request and so on. So in the ideal situation where you have a community and not just a project on GitHub, it's okay because you have procedure and so on. But when it's just a random code that you put on GitHub, then it's a bit more fuzzy because sometimes you have issues but you don't know if someone is responding and so on, so. But it's a bit not a question of Git, I'm more discussing the core of the MOOC, maybe [laughter] and not the Git.

Marc: 00:38:03.127 Yeah. I don't think the no community project and no maintener projects are in scope for the MOOC. I would actually discourage newcomers to FLOSS to get near a project that has no maintainer and no community.

Xavier: 00:38:23.507 But that said, it's important to be able to recognize them. It's something we touched on last time. It's like looking at the number of merge and etc.

Marc: 00:38:31.445 Recognize but don't get near if it's that kind of project because you will lose a lot of time and you will get no help.

Olivier: 00:38:43.431 And is this something implicit there that you tell us or is it something explicit that you want to tell people in the MOOC?

Marc: 00:38:57.151 No, it's explicit because this MOOC is basically contributing to FLOSS. So contributing means interacting with the community, basically, to improve on something existing, used, and established.

Olivier: 00:39:14.608 Okay. So do the case where you are interested in some nice tool that is unmaintained for 3 years and you find that you're the only one person that needs it and you don't know how to try and take over the maintainership, is this something that is in the scope of not?

Marc: 00:39:39.160 Probably not.

Olivier: 00:39:41.393 Okay. There could be an advanced MOOC here that is salvaging neglected or [inaudible] projects.

Xavier: 00:39:48.336 Yeah. Because that's kind of level two, is that it's more than contributing. You're basically taking over the project, becoming maintainer, forking, [inaudible]. I mean, it might be worth mentioning it though, as a possibility and etc., but then mentioning, "Okay, you might want to do a few contributions on your own. I'm getting used to how our first project works before trying that." It could make sense.

Marc: 00:40:12.369 It will not just be taking over and coding on it. It will be basically having to recreate a community around this project by yourself, which is a completely different also level than just integrating an established community.

Olivier: 00:40:29.861 Okay. So in this aspect, I think it would be important to be able to recognize such projects by looking at the tools that you have in GitHub or GitLab or whatever. Like a home, is it well-maintained or not? I don't know if you identified that already in the--

Rémi: 00:40:47.676 Yes. Yes. I think in the-- I think it was [crosstalk] first.

Marc: 00:40:50.889 First week.

Rémi: 00:40:53.513 Yeah. The properties of FLOSS project, and some of them were the activity of the project, how big the community it is, is it reactive or not?, etc., and so on.

Olivier: 00:41:06.282 So in terms of Git, looking at the graph or the network, I don't know anymore on GitHub, is one of the key tools?

Marc: 00:41:18.803 Could be.

Rémi: 00:41:20.873 Also all kind of tools, they have to know the activity, the number of issues, commits, number of contributing people [crosstalk].

Marc: 00:41:32.741 The first thing I look at in a GitHub repository is date of latest commit.

Olivier: 00:41:39.183 Yeah. Yeah. But maybe you have to look at the network to see that, oh, someone already forked it and continued the maintenance and we are just directed to the old URL and not the new one. Something like that. So that's why I think it's important to look also at the graph of the forks or the-- I don't know if you know well the tool in GitHub. I don't know if GitLab has such a tool.

Rémi: 00:42:07.803 Yeah. It has a list of forks if you click on a fork.

Olivier: 00:42:10.751 And can you see which one was modified more recently?

Rémi: 00:42:18.287 Visually you mean?

Olivier: 00:42:19.964 Yeah. Because in GitHub it's quite obvious to see, "Oh, that one has 10 commits after the latest version." So it's probably where it happens.

Marc: 00:42:29.488 You can sort by last updated.

Olivier: 00:42:33.863 Of course, we need a good project to have a look at because in GitHub maybe not all projects are forked, so.

Xavier: 00:42:41.085 Yeah. And also I think we'll need to be careful about one thing, is to not confuse people for the general case where the main URL might be the right one. So I think it might be something to consider if arriving on the project, we realize, "Okay, there hasn't been any commits for a while, no merge requests," and etc. Often, that's when it becomes useful to go, "Okay, is there someone else maintaining this now?"

Marc: 00:43:05.654 Yeah. But in the general case of established project where the communities are canonical, URL is the first one you find. You go to the project web page and the project web page still, "Okay, this is GitHub/my project/my project," and that's [where?] you are. And if the canonical website that comes up first when you look for a project guide you to an absolete repository, then there is a problem with the project and as a newcomer in the first community, you probably want to find something easier or more up-to-date.

Xavier: 00:43:47.339 And also maybe to finish because we were talking originally about workflows, and I think that is definitely something important. Yes, there's GitLab, the merger request, and etc., but there is often an assumption by the project or by the maintainers as to how it's going to be done. Like at the very minimum, reading the contributing that MD. Often, there is-- I don't know, signing a CLA. So be careful for that before opening and pull request to certain things and etc. And I think it makes sense to definitely talk about that and talk about it as much in detail as we can because, of course, that will change on each project because that's often one of the main failure points with new contributors, is that they don't actually follow the process. They don't look at these kind of things. And the contribution ends up being discarded because of that, so.

Marc: 00:44:50.266 Yeah. Yep. Actually, I had an interaction this morning like seven hours ago with someone wanting to fix something for Inkscape project, and they came to IRC and [are?] able to log in the chat. They basically say they are used to Gerrit or something like that and are not used to GitLab and GitHub workflows and tried to push to the main repo and asked for help and it went quite well. We just gave them very easy steps and they were able to follow them, so.

Rémi: 00:45:40.324 So basically, it's the fork and merge request workflow. He misses the fork.

Marc: 00:45:49.416 Yes. So it was very--

Rémi: 00:45:59.163 Yeah. That's why maybe the name fork can be a bit frightening because if you hear, "Oh, to continue to my project, you have to fork it." "No, no. I don't want to fork it. I just want to be nice to you. [laughter]

Marc: 00:46:10.835 Yeah. But fork in GitLab and GitHub is as I said, just a cloud clone. But steps to actually contribute on a GitLab or GitHub project can be very simply defined. And these platforms actually make it simpler to contribute, and they even give you the create a merge request URL with the Git protocol from the Git answer when you push.

Olivier: 00:46:53.638 Which is bit weird at first time because people that didn't use GitLab already, for instance, they are concerned when they see, "Oh, I pushed and it failed." "No, no. It didn't fail. It's just a message telling you that you might want to ask for a merge request once you have pushed it." And people don't always expect if they just used Git locally or with another platform. Anyway.

Marc: 00:47:27.524 But GitHub does the same, right?

Olivier: 00:47:30.776 I'm not sure. I'm not using GitHub so much these days. [crosstalk]. There is some confusion between GitHub and GitLab for newcomers. So that's maybe also something where if we use our own GitLab installation but what we tell people about is about GitHub, I don't know about--

Marc: 00:48:02.973 We can tell that github.com is basically a modified GitLab instance.

Xavier: 00:48:11.645 Well, I think people will know the-- actually, I think it's quite important to mention that GitHub is proprietary because there are way too many people in the FLOSS community who just use it like if it was part of the ecosystem. It's really not. And then I'm guessing in both cases we can probably have videos that show the workflow. Like take an example of creating a contribution somewhere and that then they are able to follow that if they want.

Marc: 00:48:41.128 Yeah. And what do we do of other platforms like Launchpad, or Gitea, or?

Rémi: 00:48:50.840 I was indeed looking at this survey, 'Why Do People Give Up FLOSSing? A Study of Contributor Disengagement in Open Source', and some of the result-- in some of the results they say that, "So I gave up on that project because they changed the platform or they moved to a private repo." Okay, so this is also a platform change, maybe. Of course, many of them, because they didn't have time, and changed job, and etc. But indeed, changing platform seems to be a big issue.

Olivier: 00:49:39.582 When Git appeared, after a few years, there were long discussions in some projects about, "Shall we change from CDS or SVN to go to Git?" and so on. And there were many [tales?], many flame walls to be told about those times. I'm thinking about the GNOME project, for instance, that tried once and failed, and started over, and so on. And it was quite difficult because at the time, of course, the Git support wasn't that good, and so on. But it's quite a challenge when you have your community and people have a habit, and to change tools. And even though Git is not centralized by itself, but the problem is that Git, the issues, the merge request, the support for code reviews, all the bots that will pass the test for you, and so on, so.

Rémi: 00:50:46.507 So Angie was talking about this project they have at Framasoft on a protocol for the federation of Git platforms. That will be the future. So it's based on activity pub.

Olivier: 00:51:06.337 Yeah. There may be many projects [crosstalk].

Marc: 00:51:09.617 I think it's forgefed.

Rémi: 00:51:12.700 Forgefed. Yes.

Olivier: 00:51:13.203 Oh, that's maybe the protocol. I've been working on forges 15 years ago, and we were thinking about tools to allow people to migrate from one forge to another one. So that's a new option, which is a federated like a [inaudible].

Xavier: 00:51:35.641 But in any case, within our MOOC, we people won't really be able to solve that problem. I think that we'll have to deal with the fact that there will be many forges. And maybe that can be a point where we can use contributions too. We can start with showing the workflow or explaining the main ones, and we can have a call for contribution if there is another platform that you're familiar with like Launchpad, record a video or other explanations for that one. And that will get progressively full as people do that, I guess.

Marc: 00:52:11.774 The main ones are definitely GitHub and GitLab.

Rémi: 00:52:15.110 So should we exclude any project that doesn't use Git?

Olivier: 00:52:23.716 Are there any more people not using Git? [laughter]

Xavier: 00:52:27.738 Well, there's a few. And I don't think we should exclude it. We can always make recommendation. "Okay, if you're new with this, Git is going to be easier. It's used by everyone. But if your project use something else and you really want to contribute on that, you can do it. It's just you're going to have a lot of time and maybe contribute for other people afterwards what you learn as [inaudible]."

Marc: 00:52:54.276 Scribus is on SVN.

Olivier: 00:52:58.832 And FreeBSD, is that still on RCS? I guess they're on Git now too.

Rémi: 00:53:10.481 And I remember the first brainstorming, I couldn't remember, but we had someone saying that the module 012 could be optional even. So if you already know how to do Git, you already know how to-- maybe you know a little bit about FLOSS, but you want to find a project, get in touch with the community, and do something for the project. So you want to go directly to number three module.

Marc: 00:53:45.356 You can skip to the evaluation and just do it.

Rémi: 00:53:48.152 Right. So that means that maybe-- so what are the standardization of contributing to a project if even we don't use Git, or? I don't know how to explain.

Xavier: 00:54:15.277 Oh, I mean, I think we can make some assumption, or in our case is saying that we use Git and with the main example, we use Git. But then there will be the warning for people who contribute to something that doesn't use that, that it might be different. But the principles will remain the same. So I think that's fine to not try to cover every single case, especially also from the beginning. That said, for people who skip, even if they know Git, they might not necessarily know merge request. They might not necessarily know what they have to check at the beginning before doing a first contribution. So we might need to be careful about that. Is that we want really what's really in each of those and what they need to actually be able to pick a project, do things quickly, and etc. because knowing the technical part is not enough.

Olivier: 00:55:16.821 I tend to think that Git is probably these days so common that once you're on it, you're safe and you can adjust to other tools. Or maybe if you [inaudible], you would be able to adapt somehow. But what is more difficult is how do you communicate with people? Some projects, they just use issues, others they use mailing lists. But mailing lists are just for people like us in their 50s.

Rémi: 00:55:44.859 So this is [crosstalk] of the module four.

Olivier: 00:55:47.635 Yeah. And there are more diversity in finding the venues where things can be discussed. And in the end, if you want to do a merger request and you pop up from nowhere and so on, or maybe it's sufficient that people will accept your contribution readily. But most of the times, you have some kind of a discussion going on because it's very unlikely that you're on the perfect version with a perfect batch, with a perfect use case described, and so on. So if people are not welcoming, there's always a kind of a tendency to discuss somehow. And that way, being able to discuss. Explain where I am from, what is my aim, or why do I bother people with my problems, and so on. So it's more important and then there is more variety. More diversity. And so I guess Git is the easiest one because what you learn with an example project is probably transferable to many. But if you are looking to, "Who may I ask? Are there in charge? What will they ask for me to do?", then it's more diverse. So Git is the easier one.

Xavier: 00:57:00.490 Yeah. And for me, that equals a feeling that I have from the beginning, is that, yeah, finding the project and talking the social aspect is the hardest part. The part that takes time. And that it would make sense to really try to light that as early as possible and get people to start looking at that as early as possible because that's going to be-- that's the part that everyone [blogs?] on. And again, it also depends on third parties who might not have time to answer immediately exactly at the time that we want. So yeah.

Rémi: 00:57:38.735 So maybe the second module where to find [first?] projects should not focus on how to use this kind of platform but more on how to find a project. But this is module three.

Marc: 00:58:00.137 We can talk about finding a project in both. This module two is obviously where we will present GitHub and GitLab, and we can tell that many open source projects are on these platforms and you can already browse them to see which one you like, which one are active, which one you know, and try to take-- from the week, I think, one, we said that they should keep in mind that they should think about a project they want to contribute to. So when you [create?] those, we can tell, "Filter them by the languages you know or want to use. Filter them by activity. Try to see which one you see are active." Even if you are not already engage in conversation, you can look at the open merge requests and see how people are answering and how the dialogues are working, basically. Even if it's not your patch, you can look at the merge request and see, "Oh, they ask for reformatting this file because it did not have tabs but spaces," or the contrary. And that's what's normal for contribution. I don't know if that's enough at this point.

Rémi: 00:59:25.589 So I think the-- so it's not only where, but we were more focusing on how. Talking about merge request and workflows and stuff is a little more--

Olivier: 00:59:40.325 In real life, my problem is not to find projects. Projects find me. But then [laughter] I have a [inaudible].

Marc: 00:59:48.206 Yeah. But you're not a student.

Olivier: 00:59:50.519 Okay. So I'm not the typical customer, but the how it is always different even if you manage to find where. Or if the way found you. [laughter]

Rémi: 01:00:10.895 So that's where we need mentors, as you as you said, Xavier. You have so many possibilities on how to when you have to find a project, you are in contact with the community, but then you need a little bit more help to personalize, to help you on really how to do it.

Marc: 01:00:35.900 Now we need a flowchart. [laughter]

Xavier: 01:00:40.301 But it's true. The mentors really help accelerate things and remove some risks.

Marc: 01:00:47.325 Some barriers.

Xavier: 01:00:48.612 Yes. And barriers. But that said, I think a way we can also help with that, especially for people who won't be able to have mentors, so people who will be more using the plain version of it, is to make them try things because the more they try little things at the beginning, like I don't know, even if it's just, "Okay, find the IRC and ask there." In some project it will work, in some it won't. But if they have done that, it doesn't work, they have time to see it. They can try somewhere else. Or is there a forum? Or can you find another project? Because sometimes the project is dead and that's how you realize it. So the more we can get them to try little things along the way, the more by the time they actually need to really ask something, they will already know the people, they will know how, they will know what works to interact with that project.

Rémi: 01:01:44.784 Yeah. Yeah. But that's kind of what I meant with the flowcharts. We could have a flowchart thing. Filter by the languages you know then filter by whatever. And then, "What is the date for the last commit?", "Is there an answer in the pull requests that are open?", etc., etc. And "Is there an IRC channel?", and.

Xavier: 01:02:11.420 I think the flowchart is useful, yeah, to establish what's the thinking approach with that. But I think on top of that, what's often very hard with people is to actually like, "Okay, I'm going to go and ask a question and post something publicly." And if they imagine that, "Okay, I have the flowchart, so at the last minute when I have my question, I just need to forward that and immediately I will have the answer," that might not work. So that's also those steps that are--

Marc: 01:02:44.150 In the flowchart, stay connected for 12 hours. [laughter]

Xavier: 01:02:47.971 Yes, pretty much.

Rémi: 01:02:50.961 I had this problem because I was using a project, I had an issue, so I posted an issue. Six months after, no activity on the project so I decided to fix the issue myself. It took me maybe three days. Then I did a merge request and maybe five months after, the maintainer was-- so it was 1 year until the maintainer decided to comment on my merge request. Not so much activity, but anyway.

Xavier: 01:03:29.300 Yeah. And already, you were lucky that you have any answer at all because sometimes with this kind of projects, just nothing happens, so. Yeah.

Olivier: 01:03:35.792 And there is my own experience where you're wondering how to do something and you find out that it's buggy, so you make a bug report, and 8 years later, you find that something is buggy, you search for an answer and are pleased that someone reported it. "Oh, I will have the answer." Oh no, it's you. But [laughter] you forgot that you filed it initially and nobody answered in 8 years. I should be able to find out real bugs in [inaudible] for that if I was curious enough, if you need some material for the movement. I'm not sure we have to teach people with worst case scenario. [inaudible].

Xavier: 01:04:17.901 Actually, I was about to say that those are really interesting examples. And for people to be aware that it happens like that might be good because they will run into those things where nobody answers, where there is vague thing. Especially nowadays where we are always expecting to have a support answer within 24 hours or I pay for a service so I get something, for people to know that this is not uncommon and that if this happens, they shouldn't stop support that and maybe find something else and etc. might be important because I think in a lot of cases, people are like, "Okay, I did something wrong. I reported that and the way I reported it or my patch was not good," when in a lot of cases, it's just it was the project or maybe the wrong place to report it in the project. And so they don't end up with the wrong conclusion about what they did.

Marc: 01:05:09.636 And sometimes, the time expectations are just wrong. It's especially visible when you have web bridges to IRC. And if you're on an IRC channel that bridge to the web, you know you will have users coming asking you question, leaving after one or two minutes, and no one is answering you and they're [crosstalk].

Olivier: 01:05:37.914 Which projects are still using IRC?

Rémi: 01:05:41.823 Many.

Marc: 01:05:42.649 Many.

Olivier: 01:05:43.898 Come on. It's been 25 years since [laughter] I've actually convinced people that if it didn't happen on the meeting list, it didn't happen. I mean, come on guys. You're still on synchronous discussion forums for managing a community?

Rémi: 01:06:01.802 Come on.

Xavier: 01:06:02.977 No. For answering users. [laughter]

Olivier: 01:06:05.757 I know. But if we agree that that's understanding that there are probably several ways to contact depending on your expectations on the time of response as well is probably something that we should exhibit. We should explain.

Xavier: 01:06:28.217 I agree. And even the whole concept of synchronous versus asynchronous is something that most people haven't even really thought about. You just have to see the number of people who just their company use Slack to replace email. They haven't really [inaudible]. So yeah, I think maybe a section about that would make sense.

Marc: 01:06:52.346 Maybe not.

Olivier: 01:06:52.559 [inaudible] there is always the--

Xavier: 01:06:53.406 I think we are stepping on other weeks.

Rémi: 01:06:58.803 Like the number four. Getting [crosstalk].

Marc: 01:07:01.135 Yes. Yeah. So could we list just types of skills that we want to teach or evaluate in the Git, GitHub, GitLab?

Rémi: 01:07:22.036 Fork projects, clone it locally.

Marc: 01:07:27.159 Well, if we go for no command line and just through GitLab, then clone it and forking is basically the same thing.

Rémi: 01:07:36.489 Yes.

Olivier: 01:07:38.624 I think before that, before doing stuff, it's important to be able to understand what happened in this project. Where is the Git? What is the branch? Is it working? Should I be looking at which branch and so on? How do I find the version that I downloaded or the version that is begin by [inaudible] package? How do I identify the particular tag that corresponds in Git? Stuff like that. So before doing stuff, understanding, navigating, or what we mentioned earlier, finding the right clone where it happened. So the right fork which is still active. And then you can try modifications. Then okay, you fork, you change, or you do it in the web interface that is doing that on the platform on the cloud. And then you tell people. Or maybe you tell people before doing it because in principle, it's better to have an issue before trying to patch and maybe the merge request fixes the issue.

Rémi: 01:08:39.997 So it's do and understand what you have done or understand what you will do and do it.

Olivier: 01:08:49.972 There are high chances that if you don't understand where to start from, you won't be considered. Or you will be able to rebase or stuff but it's not going to make it easier. Okay? So I guess understanding where you should try and [inaudible] is probably the first item. So some kind of transferability between, "Okay, I downloaded that ZIP version for my Windows machine," or, "I did APT in style and I kind of go in the about menu and I have a version and, okay, I want to translate something that is missing. And where should I go?" And maybe it's finding the repo to be able to fork it is already a challenge. Maybe it's overlapping with other weeks, of course, but.

Marc: 01:09:45.230 Finding a repo could definitely fit here. And navigating things at repo.

Olivier: 01:09:53.069 And you [got more than?] finding a branch. Finding a branch, and.

Rémi: 01:09:58.898 Finding the code. [crosstalk].

Olivier: 01:10:04.837 And sometimes, there are all these project based on Node.js and stuff where the cost is hidden behind a layer of conflict files and whatnot. And doing [inaudible]. Yeah, doing [inaudible] to find something. So [crosstalk]--

Marc: 01:10:26.017 Navigating within the code is in topic of a whole week.

Rémi: 01:10:33.846 But finding, at least at the beginning--

Marc: 01:10:36.848 Yeah, finding the repository, finding the branch core is definitely a Git skill. But navigating within a repository is not specifically a Git skill.

Olivier: 01:10:48.423 But before doing the [inaudible] locally, you can use the tools on the forge to do the same. So it's in between. It's not really Git but because it passes also the issues and stuff, but it can save life. Or I'm not in the good repo because it's not the repo of the documentation. There is not a repo for documentation. Stuff like that.

Xavier: 01:11:11.404 And also GitGrab itself is more useful than just Grab in a repository. So there is a little bit of a link with Git too.

Rémi: 01:11:28.470 All right. So we have 9 minutes. It's been already 72 minutes we're talking about this module number two with lots of ideas. But yeah, I think [foreign]. What is the agenda again?

Marc: 01:11:48.233 It's in the chart.

Rémi: 01:11:50.743 So because we have 9 minutes, let's go through the points. The other ones were external resources should we use? So we have a little list that I put on the front of [inaudible]. [crosstalk].

Olivier: 01:12:07.744 Something I find interesting for Git and to explain how projects evolve, maybe you mentioned it earlier, do you know, Gource, the visualization of repositories?

Rémi: 01:12:19.798 No.

Olivier: 01:12:22.340 So that's very visual. You can have a look at videos of evolution of projects over time with [crosstalk].

Rémi: 01:12:30.920 Yeah. I saw many videos on YouTube on any kind of trend.

Olivier: 01:12:35.343 Yeah. So the visualization may be confusing for people if you show something animating and so on, but still, that can be a great tool just to illustrate somehow--

Rémi: 01:12:50.071 Look to introduce the concepts. Yes.

Olivier: 01:12:51.863 --many people doing stuff over a repository, many maintainers, and they come and go, and so on. And the question of evolution in time and so on. So I think it's a great resource for getting some hints visually.

Marc: 01:13:08.466 Now I'm curious. [laughter]

Olivier: 01:13:11.566 You can search for Gource and Linux Channel, for instance, and I can show you.

Rémi: 01:13:17.700 On Inkscape?

Marc: 01:13:18.987 No, I'm doing it APT install Gource now. [laughter]

Rémi: 01:13:24.277 On Vimeo, I see a lot of Inkscape project history with [inaudible]. Two of them actually from starts from--

Olivier: 01:13:34.232 With? [inaudible]. What's that? Oh, I expected to have a direct YouTube URL. That's not what I got. Can you share a video in this DBB? Because in DBB you can share [crosstalk].

Marc: 01:13:47.443 Yes.

Olivier: 01:13:49.306 But maybe in the recording it won't.

Marc: 01:13:52.336 It will show. It should show up in the recording, I just made you presenter.

Olivier: 01:13:56.062 Okay. So I can try that there. It's not the Vimeo one but okay, let's try that. So I've just shown that to my students while doing a Git lesson the other day.

Marc: 01:14:14.374 That's Linux, right? [laughter]

Olivier: 01:14:16.286 Yeah. So actually, it's not Git initially because it was BitKeeper or SVN or FCS or whatever. But it's the same. I mean, the ideas are the same. You see the commiters that commit stuff in various directories, various modules, various [arrangements?]. And if we fast forward a bit, we can see how it evolves in time and we can see that the frequency of contributions is higher and it's more complex and so on. So I don't know if it is valuable for contributors but somehow, if you think of-- and the Linux Channel is just not the right example. But still, it gives some sense working together, and the kind of conflicts that may arise and the need for some workflow, some process. That's it.

Xavier: 01:15:23.127 Yeah. It's really nice, honestly. It would be great to include something like that because it's true that seeing a repo is really non-visual at all. And at least here you get to get to sense for the amount of people contributing on it at the same time, which is good.
[silence]

Marc: 01:15:55.741 So what's next? Yeah. So is it okay for you if we go to the idea of evaluating stuff through GitLab, maybe with LTI, or should we--? So we don't have command line, right?

Xavier: 01:16:24.168 I mean, if we do it with GitLab, people can use the command line on their side if they want to, but they don't have to.

Marc: 01:16:31.755 Yeah. But that means we don't teach the command line. So we show the command line, or?

Xavier: 01:16:42.726 I mean, it could always be an optional thing. And some of the resources like most of the games, the two games that we mentioned, they both show command line thing. So I don't think it's necessarily a bad thing but if we can make it optional, that might be a good thing.

Marc: 01:17:00.650 And they are good resources in Rémi's MOOCs to learn the command line with WebLinux and stuff. So there's always a link that we can add to people who are in need of more training.
[silence]

Rémi: 01:17:26.278 So as an external link if we really need the basics of the command-line interface, of course, we have my MOOC.

Olivier: 01:17:43.676 We cannot cover everything. It's like if people want to know more about Git, there will be the Git MOOC that will be complimentary somehow, and.

Marc: 01:17:54.032 [inaudible] main question is, what do we teach and where do we stop details?

Olivier: 01:18:01.530 You may teach the command line if you want to experience stuff like Grab and stuff and how developers can be more effective. But not really teaching, is just a matter of saying that the Unix philosophy with pipes and stuff and modularization somehow have legs in many projects maybe and influenced. I don't know.

Marc: 01:18:29.811 That's something I would be interested to hear your opinion in week six, I think.

Olivier: 01:18:40.962 Okay. I can stop this sharing. Yeah.

Marc: 01:18:46.896 Basically, using Vim and GitGrab versus using an IDE and what is brought by each approach. Even if often it's more the choice of the project what they recommend and what they do rather than each programmer, but.

Rémi: 01:19:12.169 When you talk about advanced tooling: [inaudible], make, build, con analysis, etc. If you don't know what is the command line, then.

Olivier: 01:19:26.647 And if you don't know what is Git either because nowadays to deploy an application is just pushing somewhere, a Git URL and clicking on a button.

Marc: 01:19:38.586 It's Docker Compose app.

Olivier: 01:19:41.583 No, no. You just click on the docker-compose YAML and you drop it in the web interface.

Marc: 01:19:53.401 No. It's a green button and GitHub actions. [laughter]

Xavier: 01:20:00.253 At the same time, when we were talking about the Gitpods, even for stuff like advanced tooling, if we set up something related to the project to show at least that the minimum of what people need to know, we could have those. Obviously, there will be a little bit of command line related to that, but we can also make it easier if we want to. Yeah. Thanks to that.

Marc: 01:20:22.850 I'm not completely familiar with Gitpod, so yeah.

Xavier: 01:20:27.132 Me neither. But if you can include your own VMs and etc. in it then you can probably set up what you want there. I'm guessing that if a project can use it to set up their whole development environment, there is something we can do there. But yeah, I don't know either.

Olivier: 01:20:48.172 We might as well use our own hosted Eclipse Che server. That is doing the same as Gitpod for GitLab somehow if we needed it, and if we manage to fund it.

Rémi: 01:21:03.952 Gitpod is open source?

Olivier: 01:21:07.075 Excuse me?

Rémi: 01:21:08.115 Is Gitpod open source?

Olivier: 01:21:10.473 No. But it is based on Eclipse Theia and other tools and they are contributing to Eclipse Che. So that's--

Xavier: 01:21:19.574 It's not open source Gitpod? Because they have [crosstalk].

Olivier: 01:21:23.417 Well, they open sourced it recently, maybe. Yeah.

Xavier: 01:21:27.070 Ah, okay. Because [inaudible] you can fork it, so.

Olivier: 01:21:31.005 Yeah. Because they secured funding and so on and the partnership with GitLab helped them. Well, it's evolving these days, so what was not true six months ago is different now.

Marc: 01:21:45.858 It's complicated. They have a-- not multi-license, but some files are under a proprietary license, and.

Olivier: 01:21:53.624 Yeah. It's like Visual Studio and VS Code and stuff like that. It's always [inaudible].

Marc: 01:21:58.640 I think it's more like GitLab. They have some enterprise features under a proprietary license.

Xavier: 01:22:09.885 And what was the alternative that's purely FLOSS that you were mentioning earlier there?

Olivier: 01:22:13.895 Eclipse Che. So Che is not Che Guevara, but it can also be interesting discussions. It's Eclipse on Kubernetes. It doesn't mean anything but we don't have time for me to detail.

Marc: 01:22:38.181 I think we can stop the recording now because we talked a lot about Git and GitLab and GitHub, so we have now a lot of ideas and we have now a good idea of what we could do in this part of the MOOC. So I think it's a good thing for the progress of this. So thank you, everyone. And I will stop the recording and then yep.

Rémi: 01:23:12.383 See you in the next session then.

Marc: 01:23:13.784 See you in the next session in a few days.

Olivier: 01:23:16.552 Yep. See you. Bye-bye.

Rémi: 01:23:19.992 Merci encore.
