# Module 5 - Tools

<!-- TODO: Write an introduction -->

## Setup a development environment

This section describes how to setup your computer to contribute to open source projects. If you are confident that yours
is already properly setup, feel free to skip this section!

Because it is impossible to write a tutorial for all the situations, we have a list of advice to configure a development
environment.

### Operating System

A lot of open source projects targets operating systems using the Linux kernel. If your development computer is running
Windows, it could be hard to build and run these projects. In this case, we recommend that you configure the WSL
(Windows Subsystem for Linux) or you run Linux in a Virtual Machine.

The documentation to install WSL on a Windows machine can be found
[here](https://docs.microsoft.com/en-us/windows/wsl/).
If you choose to run a Linux virtual machine, we recommend that you install [VirtualBox](https://www.virtualbox.org). A
Linux distribution that is easy to use is [Ubuntu](https://ubuntu.com).

If your machine runs MacOS or Linux, most projects will work just fine.

### Text editor

We will need a text editor to write and search through code. If you already know what is the best text editor for you,
that's perfect! However, if don't know what editor to choose, we recommend you to install [Visual Studio
Code](https://code.visualstudio.com). This editor is very popular because it is powerfull while beeing easy to use. It
has a lot of extensions.

### Account

Finally, for most projects we will need an account on the platform hosting the project. Please make sure that you have an account on
the website hosting the project (Github, Gitlab, Bitbucket, etc).

## Build and run the project

1. Install the requirements.
1. Build the project.
1. Run the project.
1. Run the tests if the project has some.

Reminder: how to get help: If it's documented, follow it (and if the documentation is wrong, then fix it ☺ ); if it's not, then
it's ok to ask about it : you don't know things so you have the best perspective to write a newcomer documentation

## Reproduce a bug

Tips to find a good bug.

Reproduce a bug.

## Report

Report what you did in your Gitlab repo.
