You might notice some tendency to put french-speaking resources here. Sorry!
Please feel free to link to english translations and/or to also submit equivalent 
english-speaking resources.

Also, please mind the license while browsing those: few of those are CC-BY-SA
or relicensable as CC-BY-SA, so while it is possible to draw inspiration from
anything, it is not acceptable to copy licensed contents into the MOOC.


# MOOCs

 * https://www.coursera.org/specializations/oss-development-linux-git#courses
 * https://librecours.net/parcours/upload-lc000/
 * https://mooc.chatons.org/

# Courses

 * Paris Diderot - Stefano Zacchiroli - https://upsilon.cc/~zack/teaching/1920/loglib/
 * Paris 8 - Pablo Rauzy - https://pablo.rauzy.name/teaching/2016-2020.html#ddll
 * http://www-public.imtbs-tsp.eu/~berger_o/csc-4522-2019-berger/
 * https://docs.openstack.org/upstream-training/ from @dachary
 * Linux Foundation - [A Beginner's Guide to Open Source Software Development](https://training.linuxfoundation.org/training/beginners-guide-open-source-software-development/)
 * Winter of contributing / [Open Source section](https://github.com/girlscript/winter-of-contributing/tree/main/Open_Source)

# Movies / documentaries

 * https://www.labatailledulibre.org/

# Books

 * https://framabook.org/logiciels-et-objets-libres/
 * https://producingoss.com/
 * https://framabook.org/docs/stallman/framabook6_stallman_v1_gnu-fdl.pdf
 * http://www.droitdeslogiciels.info/
 * http://www.catb.org/~esr/writings/cathedral-bazaar/
 * http://open-advice.org/
 * *Hackers*, by Steven Levy
 * https://gabriellacoleman.org/Coleman-Coding-Freedom.pdf
 * https://framabook.org/histoiresetculturesdulibre/
 * https://hintjens.gitbooks.io/social-architecture/content/
 * https://www.eyrolles.com/Informatique/Livre/utopie-du-logiciel-libre-9782369350972/
 *  

# Lists of FLOSS Projects
 * https://sill.etalab.gouv.fr/fr/software
 * https://framalibre.org/
 * https://directory.fsf.org/wiki/Main_Page
 * https://www.openhub.net/
	
# Communities / Discussions / Forums
 * https://talk.libreho.st/t/contributing-to-free-software/403/8
 * https://framablog.org/2020/12/08/bilan-des-actions-de-framasoft-en-2020-hors-confinement/#comment-84046
 * https://discuss.openedx.org/t/open-edx-engineering-onboarding-courses/4096/2
 * [Open Source Guides](https://opensource.guide/) by Github
 * [TeachingOpenSource](http://teachingopensource.org)
