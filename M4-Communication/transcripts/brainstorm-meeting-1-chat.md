Polyedre:back in 15 minutes
Rémi SHARROCK:ok
Rémi SHARROCK:framapad link ?
Marc Jeanmougin:https://mensuel.framapad.org/p/mooc-floss-0-9lpt?lang=en
Rémi SHARROCK:sorry I have audio connection problems
Rémi SHARROCK:so every 4/5 minutes I have a 15 sec black out in my audio (receiving and tranmitting also I think ?)
Polyedre:Yes for transmitting
Rémi SHARROCK:ok sorry for that
loic:we heard you fine
Rémi SHARROCK:integrate your
Rémi SHARROCK:yes exactly
Rémi SHARROCK:https://www.researchgate.net/profile/Patrick-Mukala/publication/313254065/figure/fig1/AS:961423358304278@1606232599177/Role-transition-in-FLOSS-communities-From-Glott-et-al-2011_W640.jpg
Rémi SHARROCK:Role transition in FLOSS communities (From Glott et al. (2011))
Rémi SHARROCK:https://www.researchgate.net/profile/Patrick-Mukala/publication/313254065/figure/fig3/AS:961423358304279@1606232599420/Workflow-net-for-novice-during-the-initiation-phase_W640.jpg
Rémi SHARROCK:Workflow net for novice during the initiation phase
Xavier Antoviaque:back in a few secs
Rémi SHARROCK:ok
Rémi SHARROCK:https://www.researchgate.net/publication/41618542_Towards_a_theory_on_the_sustainability_and_performance_of_FLOSS_communities/figures?lo=1
Polyedre:Can things about burn out, etc in floss be in this module ?
Marc Jeanmougin:yes
Marc Jeanmougin:(even if it could also be in the conclusion of the whole course ^^)
Polyedre:Do you recommend starting contributing in a big community or in a small one ? does it matter ?
Marc Jeanmougin:big ones are *usually* easier to onboard (as they are more used to onboarding)
Rémi SHARROCK:Some factors which affect retention cannot be controlled: the popularity of the project, and how early in the life-cycle a developer joins [8], [9]. However, there are also measures that communities can take to encourage retention. For example, modular code and early social interactions with peers are both associated with retention [8], [10]
Rémi SHARROCK:https://ieeexplore.ieee.org/abstract/document/8811892?casa_token=A79_i5pOZ1IAAAAA:QLO11ijN2Riu1qoRjAHivemeffo1FgcjfEQ3ncBkbHjTPZwlKROwg93JVOVot9xpIiFFAXA
Rémi SHARROCK:I'm going to disconnect and connect again with VPN, trying to solve my audio pb; see you in a few sec
Polyedre:ok !
Marc Jeanmougin:https://wiki.communitydata.science/Main_Page
Marc Jeanmougin:(sorry, browser crashed)
Rémi SHARROCK:again :)
Rémi SHARROCK:because of your thousands tabs opened
Rémi SHARROCK:https://opensourcedesign.net/
