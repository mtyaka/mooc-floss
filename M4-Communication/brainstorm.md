# Module 4 - Brainstorm, Sorted by Section

* Module coordinator: Xavier
* Length: TBD

## Intro: the social side of FLOSS

Assignee: Marc
Type: video

* Module dedicated to the human interactions within a project, how people interact with each other, the relationship within a project, code of conduct, diversity issues, as well as , where they interact (IRC/chat, mailing lists, etc.).
* The majority of the effort to contribute successfully involves social interactions, rather than technical matters. It is normal to spend a lot of time dealing with human interactions, rather than solving hard technical problems.
* In the previous modules we got in touch with the project, which is a light interaction -- to be able to contribute successfully, we'll need to get involved more deeply with the community.
* A frequent mistake by new contributors is to think that just opening a pull request is enough to contribute. That misses all the work that comes before and after that, and which is highly social in nature:
  * Before, we need to figure out how the project works, what people's motivations are, what type of work they will accept in the project, etc.
  * After, being reactive to address comments from the reviewers and respond, understand what the reviewers really care about to be able to agree on a solution with them when they raise issues, 
* And often some kind of permission is needed to be able to use the tools necessary to contribute code effectively (CI, repository write access, etc.), or at least behind some kind of barriers (eg. the review of code before being merged), and you have to perform some steps to overcome those barriers.
* Discussing is a bit different because most of the times, it's possible to discuss freely without any permission - and contribute to discussions, including on tickets. So it's useful to start with discussions because there is less friction to getting started with it, too.
* It's even more important when we plan to become a long-term contributor. But even for a single contribution, that social aspect is pretty central.

## Communication within a project

### Code of conducts

Assignee: Marc

* Nowadays most free software projects and communities have them, except some small self-mediated communities that haven't had the need to formalize these rules yet. It has become one of the funding steps of a community, like the choice of a license.
* Also includes other documents describing the rules for interacting within the project, like a `CONTRIBUTORS.md` document
* The social rules to interact, they set expectations. We should not overlook them when joining a project. Project members wrote them for a reason and as a new member, it's important to understand and follow them.
* One reason to follow those rules is that they will help getting their contribution accepted -- being rude or have behaviors that distract from the value of the contribution itself can be counter-productive to the contribution goal. Many people who don't pay enough attention to how to interact with other people in a way that will be agreeable to those people, who don't take the time to learn and observe the unwritten rules, end up being ignored and ghosted. It's an open source project, so there is rarely a requirement for anyone to respond -- part of the goal of a contributor is to make others _want_ to respond and collaborate with them.
* Part of the reasons why code of conducts exist is the need to correct for a lack of diversity, which generated bias and discriminatory attitudes towards those not part of the majority. Which is usually white males in their 30s, from Europe and North America, focused mainly on tech and code. If you're not from that group, or not contributing code, it was often considered that the person was worth less, and could be treated accordingly. Just like the origin of a contributor should not be a factor of discrimination, the type of contribution should not either: code, documentation or [design contributions](https://opensourcedesign.net/) are equally important.
* They are the "explicit" rules though, they don't include *all* the rules. They usually describe more of an ideal world and rarely include practical advice, and some remain implicit, eg. around trolling, community interactions, etc. Also many situations and people within projects have an history to consider - what is applied to a given situation might differ from the code of conduct, in those cases.
  * Also open source projects are far from the ideal of always conforming to all the rules -- in every community there is tension at times, confrontations, passive-aggressiveness, cliques and disputes. Don't be surprised to find yourself confronted to those at times.
  * Additionally, if you never participated in an interest group online, with people that you don't know, it could maybe be a shocking experience in some cases. Be prepared for something that is more fuzzy, more bizarre, maybe, than what the theory from the formal code of conduct, or that can be read in books. Joining a community is like joining a new culture - it requires a lot of openness and acceptance, within reason. It's also important to not be a stickler with the rules -- contributing to a community is supposed to be fun.
* The code of conduct is the last resort when everything else didn't work. Referring to a code of conduct and respecting it is a useful step, but not sufficient by itself. You know you're doing it right when nobody needs to refer to it, i.e. when the mediation ahead of the rules enforcement is sufficient. Cf example of the LibreOffice community: they have a code of conduct, but they almost never use it.
* Newcomers to a project can be easy targets for abusive behaviors, so it's important to recognize those situations. Being new to a community, there is a strong incentive to try fit in, and it's harder to know what's exactly accepted behavior or not. As newcomers, we generally want to avoid looking confrontational or problematic, so questioning the behavior of established community members can be difficult -- for fear of getting our questions left unanswered, or our contributions unmerged. When we are the recipient of abuse, there can be a lot of pressure to just comply and go with it.
  * It's thus important to identify whether there is someone within the community who is going to make sure that people who are a bit hostile or aggressive do not bother newcomers. Having someone handling the type of mediation mentioned above can make a huge difference, and ensures that contributing doesn't become an unpleasant experience.
  * It's also important to keep in mind that feedback about the atmosphere of a community is a contribution - one of the most difficult ones, actually. It's harder for established members to remember how it feels like to be a newcomer in their community.
  * And remember that if a community is problematic, walking away is always an option, and a form of feedback by itself.

* TODO: Establish a code of conduct for the course
* TODO: Explicitly call for contributors and learners that will improve the project's diversity to join it and participate (and setup an internship with Outreachy?)
* TODO: Get feedback from participants about the inclusivity and diversity of the course -- especially the first impressions

### "How to ask questions the smart way"

Assignee: Marc
Type: mostly text

* Netiquette:
  * Making sure you read the mailing list a little bit before posting the first time
    * On the other side, *do* participate quickly after becoming familiar with the contents. Remember the party metaphore: entering the room and starting blurting anything that comes to your mind immediately, without regard for the others, is unlikely to be appreciated. On the other side, just sitting in a corner the whole evening without talking to anyone wouldn't bring anything good either.
  * Avoid big chunks of quoted text
  * Don't waste people's time, do your homework first

References:
* http://www.catb.org/~esr/faqs/smart-questions.html
* https://producingoss.com/en/setting-tone.html
* https://producingoss.com/en/communications.html - recognize rudeness, setting the tone, avoiding common pitfalls like the smaller the topic, the longer the debate, etc.
* https://datatracker.ietf.org/doc/html/rfc1855

* TODO: Use Producing Open Source Software as a handbook throughout the course, assigning chapters to read? The role of the contributor and the project manager are mirror roles: good practice as a contributor also informs how to select the project and how to behave as a maintainer -- by responding to good behavior from one side, by good behavior from the other side.

### How to find the official communication channels & processes of the project

Assignee: Kendall 	
Type: text, video?

TODO: This section would fit more the "Where?" module (module 2) -- to move there?

* How to contact the project, how people discuss in projects
* Synchronous:
  * IRC/Zulip/Rocket.Chat/Mattermost/etc
  * IRL confs/meetups
* Asynchronous:
  * ML/Forum/etc
  * The time to get a response asynchronously can vary between project, and through time: if people are in vacation or busy it can get longer, and it can happen to not get an answer in time for something we need.
* The amount of reaction (and delay in getting it) is also related to the way questions are asked, or the way contributions are presented. Communicating it better, being more aware of the maintainers concerns and facilitating their tasks are ways to get more and faster answers.

* Maybe instead detail here how different methods of communication work
  * Activity/visual novel example: Don't open an IRC channel, ask is anyone around, and leave after one minute

### Examples of interactions

Assignees: remi, xavier, marc, kendall
Type: "game" / activity

* Example of possible chats, guess what are the intentions of participants			
* Visual novel / interactive fiction ?
  * https://github.com/inkle/ink | https://www.inklestudios.com/ink/web-tutorial/
  * Different outcomes based on interactions: eg the maintainer banning you vs getting your contribution accepted
  * Contributive activity: learners devise the next steps. Eg we provide the first two chapters, and learners can contribute the third one
* Anonymised, to no point out to specific people? Unless they are well known public figures?
  * In any case, make sure to provide a diversity of honest real examples, so that people can know what to expect and be prepared for it, and don't discover it later on by themselves, once they are involved more personall
* Role playing - get learners to guess what a role played is: a company trying to get free support? A student asking for someone to do his homework? Trolls trying to harass others for the thrill of getting a reaction?

* TODO: Spread this activity throughout module 4: Start with it, to capture the attention of learners with something concrete: have one interaction, let them choose, let it go pretty badly. And then that give them the motivation to learn more about the topic through 2-3 interactive stories illustrating the different sections. (including the following sections, like introducing the diversity of contributors that can be found, and the interactions with them)

Cases/inspiration:
* Linus Torvalds says profanity in the Debian conference in an American state where people are famous for their bigotry (Linus' case is interesting to include in a non-anonymous way, because he admitted that social issues *are* an issue, and he changed some of his behavior)
* "OK" situations: day-to-day behavior of being nice to people and not making mistakes -- what doesn't make the news, and forms the bulk of the actual interactions
* Loic's example: A mentor in a Google Summer of Code project in a free-software project, tasked to select proposals from candidates. The mentor adopted a different attitude than with the rest of the project members, and was talking to them briskly. Not insulting them, but presenting a very different face than the nice, polite, attentive attitude he had with others. But nobody made any remark about it: it was not completely out of line, and yet despite having a strict code of conduct in the project, and project members with a strong will to enforce it, it was making some people uncomfortable. It was missing someone responsible for diffusing tensions, because that was not a case where the code of conduct was applied.
* Also ask the participants for examples? Some of them could then be incorporated in the course.
* Person who comes to get support and is outraged by a bug, and the lack of response 

### Diversity of contributors

Assignee: Marc	
Type: text, interviews

* Diversity of contributor types -- contributors are not just not coders, but also other people that interact with coders like designers, like bug reporters, etc. Not everyone does the same thing in a project
  * How to interact with other types of contributors
* People who stay vs drive-by contributors

## Retention in projects

Assignee: Obergix	
Type: text/video/interviews

* Contributors always end up joining for a limited amount of time - whether it's for a day, a month, or 10 years, the important is to be able to recognise the end, and to quit gracefully. Just like there is no obligation to contribute, there is no shame in leaving -- the important bit is to acknowledge it, to let other people know what to expect, and give space for others as needed. To basically be able to say, "Okay, I'm done with the project," or, "I don't have time for it anymore. Maybe I'll come back to it later", and let others know about it.
* Many contributors struggle with this -- some stay past the time where they stopped finding the project or the work interesting, and keep trying to contribute while in a "burned out" state. This is not only bad for the contributor themselves, who put themselves through unnecessary drudgery, and burn their chances of finding pleasure in contributing to the project again in the future. It's also very detrimental to the other members of the project, who have to deal with a disgruntled member -- and new contributors can be especially prone to it.

Interactive novel:
* Difference of interaction between types of members, and the state/type of their motivation: eg. contributor who has been here for 10 years; another one who has been here 2 years ago for one commit and just now is suggesting something new and not planning to stay; etc.

Interview questions:
* What did you do in the past when you stopped find pleasure in contributing to an open source project you had committed to?
* Did you have to deal with burned out contributors?

References:
* [Why Do Episodic Volunteers Stay in FLOSS Communities?](https://dl.acm.org/doi/10.1109/ICSE.2019.00100), by Fitzgerald
  * _"Some factors which affect retention cannot be controlled: the popularity of the project, and how early in the life-cycle a developer joins [8], [9]. However, there are also measures that communities can take to encourage retention. For example, modular code and early social interactions with peers are both associated with retention [8], [10]"_
* [An empirical verification of a-priori learning models on mailing archives in the context of online learning activities of participants in free\libre open source software (FLOSS) communities]()
  * [Burn outs, role transition in floss communities](https://www.researchgate.net/profile/Patrick-Mukala/publication/313254065/figure/fig1/AS:961423358304278@1606232599177/Role-transition-in-FLOSS-communities-From-Glott-et-al-2011_W640.jpg)
  * [Workflow net for novice during initiation phase](https://www.researchgate.net/profile/Patrick-Mukala/publication/313254065/figure/fig3/AS:961423358304279@1606232599420/Workflow-net-for-novice-during-the-initiation-phase_W640.jpg)

## Project get-in-touch

Assignee: remi	
Type: peer-grading activity

* TODO: Reconcile with the section in module 1? Move this there, and link back to it when people have changed project, so they make sure to do this step?

* Go to find the IRC channel (or Slack/Mattermost/Rocket.Chat/Matrix/Zulip/mailing-list/etc.), and either:
  * Ask a question
  * Say "thanks!"
  * Answer someone's question
* Make sure it's an interaction - ie that someone else responds, and that there is a little bit of back and forth. The idea is to break the ice, and keep it going.
* It's a difficult and awkward step. Often, we don't know what to ask, and it feels artificial. But it's useful to figure out how to mingle, and to mingle as early as possible:
  * It's like being in a party for the first time with lots of people we don't know. We go to someone, a glass in our hand, and we engage the conversation. The topic doesn't need to be brilliant, but it's important for the interaction to be appealing to the person we talk to.
  * The more we wait in our corner without talking to anyone, the more difficult it looks to do that. When we start talking to someone immediately, it might at first be a very superficial conversation, but the earlier we can get to more interesting topics - simply become we get to know each other earlier.
  * It's also similar to the role of networking when looking for a job. To get recommended for jobs (or aware of good opportunities), we need people to know us, to trust us -- this is also true in the context of an open source project, to be known and trusted enough to warrant the time and attention of maintainers, as well as increase their goodwill to accept our work.

* Someone needs accept our first contribution, which means that someone will need to be paying attention to what we submit when we submit it. And it's more likely to happen with people we know for a longer time: someone we've known even slightly for four weeks is more likely to accept our contribution  than someone we've met yesterday. When the time comes to accept our contribution, we need to be already part of the team somehow, even just a little bit. Being around for some time, interacting with other community members, is the simplest way to achieve that.
* Interacting with the project's community directly also develops our own understanding of the project. We are social animals, so social interaction is a huge part of knowledge acquisition within a group. By having put ourselves out there, by having interacted, we'll also adapt how we contribute, and what we contribute -- making it also likelier to be accepted than if we had waited.
  * It also develops our empathy and understanding of the individuals who manage a project. For example, an domain expert who comes to contribute to a free software project, they might assume that some the project contributors are hobbyists, and might not take them very seriously. But by interacting directly, rather than just looking at the project from a distance, there will be answers and pushback. There will be times where the expert make mistakes too -- so it helps the experts to have a more friendly attitude toward people who are less skilled, the interaction brings everyone closer.
  * There is also the opposite case, where a new contributor will assume that everyone in the project is an expert, and end up producing code that is too complicated for the project to be able to maintain. Interacting with the project members early and frequently helps to make sure that the other people from the project will be able to understand and maintain the code being produced.

* Ask the students to check if they indeed tried to get in touch ?
* The proof : screenshot ? and update your gitlab project

## Project tips

Assignee: Xavier

### Finalizing the project selection

* Last chance -- ideally participants will have already engaged with a project for several weeks at this point. They can still switch now, but it's their last chance to do so
* GO OR NO GO decision: If the project isn't a good candidate for contributions though, we want to make sure the learners can detect it, and can use this last opportunity to find a better one
* TODO: Provide checklist to grade the selected project (go through the whole course content once finished to compile it)
  * Ask for specific quantitative measurements
  * Use methodologies to evaluate the strength and weaknesses of FLOSS communities:
    * [CHAOS](https://chaoss.community/metrics/)
    * https://carlschwan.eu/2021/04/29/health-of-the-kde-community/
    * https://opensource.com/business/15/12/top-5-open-source-community-metrics-track
  * Pace of responses/interactions in the community and interaction times/timezones -- how long exactly did the project take to respond
    * It has to match the learner's timeline for a contribution -- help by calculating an ETA to contribution, based on the previous interactions?
  * Size of the community -- big ones are *usually* easier to onboard, as they are more used to onboarding)
  * Use the journal to help assess the project and decide on which one to keep working on:
    * Did the discussions/contacts/contributions in previous weeks go well? Did you get answers?
    * Provide a rubric to grade the project: a high scoring project is kept, a low scoring project leads to picking a new one -- or evaluate different projects and then pick the one that scores the highest
* Include a "project contribution health curve" -- like a temperature curve when you're in a hospital, which shows at a glance, not only where we stand now, but how it is evolving over time:
  * Are we getting more/faster reactions from the project, due to our standing improving? Are we interacting often enough?
  * Is the project answering to other people at all, or should I be patient because it isn't responsive in general currently?
  * What is the learning curve for the project? Consider both the technical and the contribution learning curves: for someone just learning how to code and trying to contribute to the kernel, it would be difficult technically. But if I'm contributing a typo fix in a comment and the project is good at managing contribution and accepting them, it might work out.
* As a backup for students who can't find a good project, mention the partner projects

* TODO: Integrate with previous modules. That needs to be done little by little, while the current module would be about final scoring, as well as redoing the evaluation with a new project if the existing ones score too low

### Contribution communication levels

Not all contribution types require the same level of communication complexity. As mentioned in module 1, we want to start from the smallest size, and slowly work our way towards the larger, more complex levels, especially with social aspects:

1. Answering a question on the mailing list or IRC from someone who asks it
1. Fixing a typo in the documentation
1. Improving the documentation contents
1. Reproducing a easy simple bug
1. Write a test that reproduces a easy simple bug (half the work of fixing the bug)
1. Fixing a easy simple bug in trivial way
1. Fixing a more complex bug
1. Developing a feature
1. Reorganizing the governance of the project

TODO:
* Reconcile with the previous modules/sections -- implement the first 2-3 steps as activities at the end of each module, to progressively ramp up the contributions?
* Make sure learners have completed those preliminary contribution steps

#### Contributing support work

* Contributing support work is one of the easiest form of contributions: it doesn't require any specific permission, and usually that's something that people who manage the project are really grateful for because there are always way too many questions to answer. It's a low-friction way to start contributing immediately, while gaining knowledge about the project, and recognition from other projects members through very visible and useful work.
* Can be done in the forum (looking at threads which are missing an answer), IRC, bug tracker (reproducing bugs, linking to duplicates, etc.)

#### Contributing to the documentation

* In some cases, it can be a great intermediary contribution step: when it is more involved and formal than contributing to discussions, but less than code
* It's an appreciated form of contribution, because that tends to be something that has less people interested in it, including from the maintainers themselves.
* It often allows to test the contribution workflow, because the documentation is often versioned in repositories like the code is, and requires similar formal reviews to get merged. But there are often less of strict constraints about it than with code
* Picking some documentation tasks can also allow to get started on learning to pick up contributions tasks in general: learning the criteria to choose them (size, accuracy, degree of support for the change, etc.), including the social aspects of it -- ie, ensuring that at least one of the maintainers will review and support the change, by discussing with them
  * One good way to go at this is to first do a small consensual fix in the documentation, to prove that you are willing to work and send pull requests, and then ask the maintainers what else needs work in the documentation, then start to work on what they mention.
* This might push you to start exploring the code, as often it is the source of truth for updating the documentation -- if you want to document things well, you need to go see what's happening.
* Note that, like for code tasks, it's important to pick a documentation task that is reasonably accessible, not one where you already need to be an expert to complete it -- at least not at first. The best documentation contributions at this stage will take advantage of your beginner point of view: someone who reads the documentation for the first time, who doesn't know anything about the project, will often be better at explaining things from a newcomer perspective, which doesn't already have all the implicit knowledge that will be obvious to the core developers, but lacking to newcomers.

#### Fixing a bug

* Break it down in successive steps -- each of which can be an individual contribution, leaving the later steps to others if they prove to be too complex. The earlier steps will already have save work for other contributors.
* Start with reproducing the bug: confirm that you can reproduce it manually using the instructions in the bug report, and explicitly comment on the ticket that you could reproduce, and how. If any information is missing in the bug report that you needed to reproduce the bug, include the missing reproduction steps and information
* A great intermediary second step, before attempting to fix the bug, is to write a test that reproduces the bug. It's like an ideal version of the reproduction steps -- a systematic implementation in code, to reproduce the bug automatically. This also saves time writing the bugfix itself, as the bugfix would need to include the test. Post the test as a patch in the bug report, or as a draft pull request.
* The last step is to finally fix the bug, which at this point is greatly facilitated by the existence of detailled reproduction steps and a test -- it becomes about getting the test to pass, by understanding and fixing what makes it fail.

### How to respond to reviews

* A simple rule applies: obeying the reviews that we get, even when we don't necessarily agree with them. As newcomers, we submit ourselves to the reviewer to the extent that our work is not degraded beyond recognition.
* Maybe we would have done things differently, but as long as the review does not go against our contribution, having to adapt to the requests of the reviewers will still bring us closer to our goal. It's better to have our a modified version of our contribution merged, rather than disagreeing and ending up with an unmerged contribution, which we'll have to maintain separately forever.
* This is a social interaction many of us struggle with at first -- it can be difficult to do something without being convinced that it is the right approach, or for matters of taste, like the style of a function. But being newcomers, we don't yet have much credit in the community or the reviewers, so accepting and applying all review comments will help to score precious points in convincing reviewers to accept the contribution.
* Once we know the project members better, and we have more credit as a recurring contributor, we will have more latitude to address controversial topics -- but we first need to prove that we can work with the existing community of contributors. This is similar to the contributions levels described in the previous section.
* This is similar to joining a new job, or a party from people we don't know: if the host asks you to help set the table, and wants it done in a specific way, it would be rude to refuse or criticize the approach. Being at someone's place and not knowing anyone, we are expected to conform to their way of doing things -- and if we don't like it, we can always go home early. After a while, once we have a good relationship with the host, maybe then we can start to say, "Hey, I'm not sure about the way set the table, maybe there is a better way?"

### Mentorship

* Last chance to find a mentor or pick a group for peer-working
* TINDER-like matching?
* Handle different types of mentors/students:
  * Mentors contributors from the partner projects (free to the students? students bidding?)
  * Mentors contributors from the course (former students helping new students?)
  * Paid mentors (course mode, linking to one of the partner projects)
  * Student's friend - someone they know that might be able to help them with contributing to a specific project, someone who they know who has a few contributions
  * Pairing the students without mentors together to work on contributing to a common project together, and review each other
* TODO: When are mentors assigned? Now is the last occasion, but should they be assigned from the beginning of the course whenever possible?
* TODO: How are mentors and learner (or learning group) working together? Use workgroups features

### Contribution journal update

* Update journal with (include links to relevant sections/modules):
  * Project (what?)
  * Locations (where?) and especially communication channels being monitored (MLs, IRC channels, etc)
    * Quantitative data about activity levels there
  * Actors of the projects (who?)
  * How:
    * How the project works (points from the current document: code of conduct
  * Potential contributions details/ideas (type of contribution, area/repos identified, core/plugin, etc.)
  * Mentor or mentoring resources if you have one already
  * Mentor/peers
* Store this data on Wikidata, as a contribution? (common parts: ease of contribution, frequency of replies, etc.)
* Peer review of the journal -- from any student, but ideally from other students paired on the same project, and/or the mentor.

* TODO: Integrate with the section about "Finalizing the project selection" above? Use the journal as the grading rubric, storing information about various projects being evaluated throughout the weeks?

## Quizz time

Assignee: Marc (probably)	
Type: graded activity (MCQ?)

## References & papers

TODO:
* Section to sort
* Research additional papers about free software communities and contributions on Google Scholar
* Contact some of the authors we find in those papers just to let them know we are working on that

References:
* [Towards_a_theory_on_the_sustainability_and_performance_of_FLOSS_communities](https://tigerprints.clemson.edu/cgi/viewcontent.cgi?article=1471&context=all_dissertations), by Mohammad Almarzouq
* [Community Data Science Collective](https://wiki.communitydata.science/Main_Page): social scientist groups studying online communities and especially open source communities like Wikipedia, Linux, etc., from North American universities.

