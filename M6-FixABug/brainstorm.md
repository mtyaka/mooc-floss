# Module 6 - Fix a bug

<!-- TODO: Write an introduction -->

## Fix the bug

- Reproduce the bug
- Write tests if applicable
- Fix the issue with minimal breaking changes
- Run Tests
- Submit the contribution

## Tips to discover a code base

This section lists some tips that can be useful to discover a code base. It is possible that some of them do not apply
to the project you are working on.

1. **Make sure that the local copy of the project is up to date**
   Before trying to fix a bug and dive into the code base, it is important to be sure that the code we are working on is
   up to date. Else, it is possible that the bug has already been fixed.

   If your project is versionned using git, here are example commands you can use. Assuming your working repository is
   clean:

   1. Save all your modifications:

      ```sh
      git stash
      ```

   1. Fetch the commits from all the remotes, this should include the upstream repository:

      ```sh
      git fetch all
      ```

   1. Apply the changes added to the upstream main branch:

      ```sh
      git rebase <remote-name>/<main-branch-name>
      ```

   1. Restore your modifications:

      ```sh
      git stash pop
      ```

      You may need to [resolve some conflicts](https://www.stefaanlippens.net/resolve-git-unmerged-paths-after-stash-pop/).

1. **Use external resources**
   Some external resources can be very useful to understand the structure of the code base. We can search if a project
   has a developer documentation or comments that explain the structure of the code.

1. **Use file and directory names**
   Good projects often have explicit filenames. We can use them to guess the global code structure and guess which files
   could be reponsible for the bug.

1. **Ask**
   A good way to discover a code base and identify the probable location of a bug is just to ask! We can ask the code
   owner if he or she exists, but also ask in the issue or in a discussion platform if the project has one. It is
   important however to not expect an answer immediately.

- Use git efficiently
- Use an IDE (goto definition, find reference)

## Tips to identify the bug

- Change something happening when the program run
- How to debug (videos ?) (debuggers, print statements, etc)

## Report

Report what you did in your Gitlab repo.
