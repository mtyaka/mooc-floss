# How to contribute?

We strive to welcome all contributors!

We want to be a project where contributing is both simple and inclusive, so please read the [Code of Conduct][code_of_conduct] (tldr: "We pledge to act and interact in ways that contribute to an open, welcoming, diverse, inclusive, and healthy community.") first. You may also want to read the [license][license] (tldr: CC-BY-SA 4.0).

[code_of_conduct]: CODE_OF_CONDUCT.md
[license]: LICENSE.md

# So, what's the plan?

We tried to create this course as openly as possible, so that even after the start of the project, it's still possible to understand the plan and the reasoning behind decisions taken. In that spirit, all our meetings are recorded and transcribed. In particular, we had two preparatory sets of live meetings about this course:

 - From February 2021 to March 2021, we brainstormed the contents of the course, aka "What are we going to talk about? How to approach it?"
 - In April and May 2021, we tried to put these thoughts into a *structure* for the course: What steps will the students have to take, what sort of activities will they do, and what we will expect them to learn.
 - From there, we are now (May 2021) focused on creating a first draft of the content, for all modules, and all module chapters. The first draft will be created in a `${week}/brainstorm.md` file, then split into the actual course structure within `course/html/` folder.

Follow scheduled meetings [here](https://gitlab.com/mooc-floss/mooc-floss/-/issues/40).

# That's great! Can I help?

Yes! In each module, each chapter has someone assigned to it. If you're willing to help create material for a course section, there is usually an issue about it. Just post to this issue, and get in touch with the coordinator of that chapter, and they'll tell you what they have already done and what you'll be able to help with!


# Communication

We communicate primarily on three platforms:
 - GitLab: All long-running discussions happen here on GitLab, whether it's for discussing a problem or a planned change (in an issue) or discussing a merge request.
 - Video chats: Most of the brainstorms and plans we've made were organized by videoconference, which is usually faster to communicate and exchange ideas than text. Since it's harder to backlog, they are all transcribed to be browsable and searchable. The times and links of those meetings are usually announced as an issue on GitLab with a deadline.
 - Matrix: We have a public chat room for more informal discussions or to exchange links, but it's less used than the other solutions. The room id is #mooc-floss:matrix.r2.enst.fr.

Communication primarily happens in English (as the course will be in English). Some French may slip in as many course creators are French.

## Who has merging rights?

All the [members of the project][members] (core committers) have the right to approve and merge the merge request. A new core member can be elected to the group by unanimous election in the current core group.

The merge request raised should be approved by at **least** one core committer.

A core committer can self-approve and merge a Merge Request _only if no objections are raised for a week_.

[members]: https://gitlab.com/mooc-floss/mooc-floss/-/project_members

## Contributing to the course

The preferred way to contribute is to [submit a Merge Request (MR) on GitLab][create-mr]. You can [fork the project][fork-project], [edit][edit-branch] in a [branch] of your fork, and present and submit your modifications by [creating an MR][create-mr].

**Do not push commits directly to the `master` branch.**

[create-mr]: https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html
[fork-project]: https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html
[edit-branch]: https://docs.gitlab.com/ee/user/project/repository/web_editor.html
[branch]: (https://docs.gitlab.com/ee/user/project/repository/branches/)

### Writing style

To unify the learner experience, there are some conventions to follow when writing parts of the MOOC:

* **How to address the learners**: when asking to do a task, use "you" (eg. "Go edit a page on Wikipedia! You should make sure to not post spam."); but for any other case where it feels appropriate, prefer to use "we", as it tends to be more inclusive: instead of being the teachers who tell students what to do, we put ourselves all in the same group/community, of contributors to free software projects (eg. "When we want to test a branch we'll typically not just read the code but also build it locally to test if the new feature works").
* **Neutrality**: we aim to remain neutral on topics that can be divisive within the free & open source community (for example, we don't chose between using "free software" or "open source" - we use "FLOSS" for "Free Libre Open Source Software"); when there are several possible point of views on such a topic, we will aim to present the different point of views, from the perspective of those who would defend it.

## Decision process

Ideally, the merging decisions will be taken based on [Consensus][consensus]. The main difference with Wikipedia is that the modifications are approved before they are merged, while wiki editing is (mostly) post-publication consensus building, with the MR page as discussion space.

In the case of major disagreements, ultimately, **for now**, the decision process ends with the <abbr title="Benevolent Dictator For Life">BDFL</abbr> process from the initiators of the project (i.e. Marc Jeanmougin), but once/if the community grows, more democratic processes could be discussed and adopted, if deemed beneficial for the project's health.

[consensus]: https://en.wikipedia.org/wiki/Wikipedia:Consensus



