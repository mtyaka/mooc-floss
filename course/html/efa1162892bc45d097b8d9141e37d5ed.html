<h4 id="the-importance-of-early-and-frequent-communication">The importance of early and frequent communication</h4>
<p>Communication is one of the most important first steps to contribute to a project</p>
<ul>
<li>An open source project is not just the code, it's a community -- even when it only has a single maintainer!</li>
<li>The first step, often overlooked, is not to try to push code: it's to understand how the project and its community works, and understand how we can fit in. It's useful to think of it like joining an organization, a party at someone's place, or an online forum. The ability to successfully contribute to the group will depend on how we approach it.</li>
<li><em>[Media: multiple illustrations showing the different scenes described, showing the effect of the newcomer attitude on the other people at a party]</em></li>
<li>We are much more likely to be well received by the group when we start by listening/reading to the current discussions, figuring out the group's dynamic and interests, saying hi, offering to help on the existing projects, using the interactions to progressively learn more and ask questions, etc. If we barge in and immediately try to monopolize the conversation with our own project or topic, it will often not match the group's interest, and could end up being ignored or rejected.</li>
<li>Talking to other people, in public, on a respected project is also something that can be a difficult psychological step - one of the hardest, especially when joining a new project for the first time. We can be afraid of bothering people, of being rejected or ignored. It's important to deal with this feeling now, and pass that step to realize that it's fine. When this is delayed, everything else is also going to be delayed because everything about contributing relies on establishing good communication. It's important to go and put ourselves out there immediately, and start talking to people.</li>
<li>Module 4 will go more in detail about communication (include link), but the goal here is to get the ball rolling.</li>
</ul>
<h4 id="maintainer-s-time-a-scarce-resource">Maintainer's time - A scarce resource</h4>
<ul>
<li>
<p>Even though we are coming to contribute, the members of the project are also contributors volunteering their time -- and have been doing so for much longer. So they don't owe us anything: we don't pay them for their work. We aren't clients, but fellow community members, and it's important to treat them as such. If anything, at the beginning <em>we</em> owe the project contributors a lot:</p>
<ul>
<li>The time they have spent building the project until we join it,</li>
<li>The time they might spend answering and helping us while we learn the ropes -- that's a precious gift, from very skilled developers, which would be expensive if we had to pay for it.</li>
<li>The time they spend maintaining the project on an ongoing basis -- including maintaining any patch that they would accept in the project, including our own contributions. It's easy to change code once in an open source project -- but having to carry it over in each release is a burden that the maintainers take over from us when they merge a patch, and that can be a larger commitment than the patch itself.</li>
</ul>
</li>
<li>
<p>The most scarce resource in a FLOSS project is the project maintainers' time. To be able to contribute successfully as a new contributor to the project, we will need to use some of their time -- at least for reviewing our work, and hopefully to get some further guidance and answers while we learn. To get the maintainers to prioritize spending some of their time working with us, we'll have to show to them that we are worth the time investment.</p>
</li>
</ul>
<h4 id="starting-as-small-as-possible">Starting as small as possible</h4>
<ul>
<li>Very often as new contributors arriving on a new project, we end up getting overwhelmed by the amount of information to absorb to be able to successfully contribute to it, and blocked by what we don't know: unfamiliarity with the process &amp; codebase, the opinion and priorities of the maintainers, etc.</li>
<li>
<p>Mutual trust, knowledge and relationships take time to build, it's important to not overload ourselves or the maintainers by aiming too high, too fast. It's similar to the reasons why agile development exists -- to be useful, code needs to answer specific needs from users or other developers, so it requires frequent communication, and iterative changes to reflect new information you collect over time. Plan work in small short iterations, and avoid writing a lot of code at a time.</p>
<ul>
<li>You might know the saying "<a href="https://blog.codinghorror.com/check-in-early-check-in-often/">commit early and commit often</a>", which recommends to create a small and frequent stream of commits while working, rather than one large infrequent commit. The idea, besides allowing to preserve a more granulary history of changes, is also to facilitate the integration of the work with other developers -- commits are a form of communication, which allows others to be aware of your current work, and reconcile theirs with yours more easily. With contribution, this is even more important, as initially existing developers are not even aware of you -- pushing code towards them, in small and frequent quantity, allows them to become aware of your work, and you of theirs through their reviews. The saying becomes "Get merges early, get merges often".</li>
</ul>
</li>
<li>
<p>We want to maximize the amount of feedback we get from the project, as this information will allow us to shape our contribution in a way that can be accepted by the project. Small iterations optimize this information collection process:</p>
<ul>
<li>The production of concrete work incentivize the project maintainers to review and produce feedback,</li>
<li>The small size of the patch from each iteration keeps the amount of effort required from them small so they will get to it faster,</li>
<li>Getting this feedback will allow us to approach the next set of changes more efficiently, as we will already be aware of the information provided in the last rounds of feedback.</li>
<li>Example of what not to do: In OpenStack, Hewlett-Packard forked the code base and worked for 1-2 years on a huge contribution, without frequent interaction. When it was time to be merged, it was a catastrophe of epic proportions to the point that it made the news, and its management had to acknowledge the issue.</li>
</ul>
</li>
<li>
<p>Keeping the size small is especially important in the initial stages, when we know very little (and don't know what we don't know), and have not yet acquired any trust from the project maintainers.</p>
</li>
<li>We also do not want to over-commit because it can turn a positive experience into something negative, when we don't actually have the time to complete what we committed to. Take on little, but do it, and then only take more once you're done.</li>
<li>Which gives the golden rule of the new contributor: <strong>The size of a contribution/interaction should not exceed the total size of our previous successful contributions to the project</strong>, building up very slowly at first from the smallest possible useful contribution.
<ul>
<li>Successful contribution = acknowledged and accepted by the project, either through a positive answer or a merge pull request. Pick the smallest interaction and contribution you can think of at first. The goal isn't to demonstrate technical excellence, or to show a lot of work.</li>
</ul>
</li>
</ul>
<p></p>