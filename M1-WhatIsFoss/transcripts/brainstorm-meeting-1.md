# Brainstorm - Module 1 (Meeting Transcript)

Video: https://digimedia1.r2.enst.fr/playback/presentation/2.0/playback.html?meetingId=73d3cd53ec8ede3f636b9a782ecc9f6375cf2271-1613064622928

Remi: 00:00:02.119 All right. So hi, everyone. Thank you again for joining this second brainstorming session, on the big project MOOC, FLOSS. And today we have the following agenda. First, we will introduce ourselves very quickly. And then we will do a kind of brainstorm, asking many questions on what we should do for the first module. So the title of the first module, could be changed also, is What is Free Software? And here you can see some of the ideas we had to start. So we have five different kind of content, and we will explain that later, I think. Go through each of them more precisely. So the agenda and everything we write will be on Framapad, and the link is on the chat. I put it back just here. All right. So everybody is allowed to write anything in this pad. So first, a very quick [foreign] - so round table, I don't know how we say that in English - presentation for everybody. I'm Remi Sharrock from Telecom Paris, an engineering school in France. And I am an assistant professor in computer science. Marc?

Marc: 00:01:56.351 I am Marc Jeanmougin. I am a research engineer in Telecom Paris, in France. I'm a colleague of Remi. And I had the idea of starting this MOOC. And that's it.

Remi: 00:02:12.943 All right. Xavier?

Xavier: 00:02:15.987 Yeah. Xavier Antoviaque. So I'm from the OpenCraft and the Open edX community. And I'm coming to this MOOC to see if we can kind of join the initiative between this and what we were planning to do on the Open edX side, to introduce new contributors to upstreaming. [Thanks?].

Remi: 00:02:40.827 I think we still have Polyèdre here.

Polyèdre: 00:02:44.018 Yes. So hi. I'm Polyèdre. And I'm a student in computer science. And here I am more like a beta tester of this MOOC.

Remi: 00:02:55.858 Very good. Thank you all for joining today. So just to give one second of context, these brainstorming sessions are open to the public. And the whole MOOC is open to contribution from the very beginning. And because it is the start of this big project, we just start to brainstorm on the content of the first module today, week 1. And the last brainstorming was about the whole structure of the MOOC. So we had many very interesting questions on how to redistribute content and how to make a new structure for the MOOC. And so thank you for joining the first brainstorming. So for this one, I suggest we go directly to the content and try to get many ideas on what we should talk about in this first week. So as a reminder here, you have the whole structure. Still I think we didn't put the new updated structure with all the feedback we had during the last brainstorming. But that will follow up shortly, I think. So as you can see here, in week-- maybe we could talk also about week 0. So in week 0, it's an introduction about who we are, what is this MOOC, how to contribute. And in week 1, the title, but again, could be modified, the title is What is Free Software? So we thought about distinguishing between four different types of content. The first is what we call transmission. Transmission is something you read, something you listen, a video, a PDF, HTML. Anything without any interaction or activity. The second one is interaction. So interaction, of course, includes the forums. Anything that could make the students interact with each other or interact with the teachers or the contributors of the MOOC. Or even interaction between contributors of the MOOC. I mean any kind of interaction or-- I don't know. Maybe interaction between the students and a simulated robot, maybe. And then activities. So here these includes exercises. Usually, they are not graded. So it means that they don't-- these activities, everything that you do don't count for the certificate at the end. So it's somehow something to do, because it's an activity, but you don't have any grade. And we could say that it is optional. So the minimal thing that you would have to do as an activity is evaluation that is take into account for the final grade. And that will count for the certificate at the end. So, yeah. One thing is that, on the platform we chose for this MOOC, you have the concept of certificates. The certificate proves, kind of proves, some set of skills. And to gain this set of skills, you have to pass through evaluated activities. So that's the last column you can see here, evaluation. And so evaluation is really something you do to get the certificate at the end. So when I say, "The minimal activities that you have to do to get the certificate," it means that if you want to get the certificate as quick as possible, you just do the evaluation and maybe you will get the certificate. And maybe because you are blocked or you want to take more time to know more, maybe you will do the activities, maybe you will interact. Maybe you will read or see the videos, the transmission part.

Remi: 00:07:35.525 So these parts - transmission, interaction, activities - are kind of optional, depending on who you are. Because maybe you know about the four freedoms, you know about the history of free software, you know everything, and you just want to go directly to the multiple-choice questions, or the activity that is evaluated. So that's how we divide the content into four different kind of contents. So what I suggest is to brainstorm. Maybe we could start with all the ideas we have on all the content that could be of type transmission. And as a start, we just wrote something about the four freedoms, about the history of FLOSS. About what is the difference between free software and open-source? But I am okay to get any kind of idea, if you have other ideas on what we should talk about to introduce the MOOC and to introduce FLOSS. Of course, we should define FLOSS at the beginning, but do much more, I think.

Polyèdre: 00:08:59.911 Maybe we could add why a FLOSS is a great way to produce software, for instance. And also some examples of great FLOSS project.

Remi: 00:09:21.508 Very interesting. So why FLOSS? And to answer that question, maybe we could have different perspectives or different angle of attack. Like you are a small company, you are a big company. You are a contributor, independent-- I don't know. So why would you choose FLOSS? That's the question you were thinking, right?

Polyèdre: 00:09:55.873 Yeah. Why should choosing FLOSS? And why this MOOC is relevant. Not about FLOSS, but trying to justify that this MOOC is important because FLOSS is a great tool. I don't know if I'm clear.

Remi: 00:10:26.071 So something like why FLOSS is important for humanity, or for different kind of society, democracy, something like that?

Polyèdre: 00:10:37.207 Yeah. And why it is a great tool to produce software. And the ethic behind FLOSS.

Marc: 00:10:47.498 Your angles are basically exactly the debate of free software versus open-source. Because free software is basically insisting on how free software can improve society as a whole. And be basically a better way of collaborating, of producing stuff, and of basically changing the world. It can be seen as, let's caricature a bit, communist propaganda. Because it's creating a common, and in the first sense of the common. A common good. Free software is a common good. And open-source was sort of coined as a marketing term to sell the ideas of a free software, but to companies. So that companies could basically take the ideas of contribution, of community, of interactions, of sharing stuff, and see that it's an efficient way to create good and reliable software, and to apply that basically in the context of capitalism. I'm caricaturing [crosstalk].

Xavier: 00:12:08.285 I think that, that also gives a good reason for using FLOSS, because actually both perspectives are useful and complementary. And probably everyone would take the approach that they prefer. But yeah. I think they are worth definitely explaining. And also, I think especially because we are talking about upstreaming in particular, like contributing to a project, it's more than just, "It's good that free software exists." It's also, "Why it's also important and beneficial for people and companies to contribute to projects." Because a lot of people can just use free software. And that's already great. It's already a step, compared to using proprietary software. But recognizing that there are some advantages, in computing specifically, I think can help to justify this specific MOOC. And again, I think those two perspectives of free software and open-source can be interesting to take there because some of them will be about changing society. "I want to give back because I want to make the world a better place." But there are also very objective advantages, including for companies that behave very egoistically. In terms of maintenance, in terms of being able to pull resources together, instead of having to replicate things. There are a bunch of things that can be said for contributing also. So.

Remi: 00:13:56.799 I see, Marc, that you are writing something there. [crosstalk] and I'm writing at the end of the pad.

Marc: 00:14:03.422 Oh, [you are writing?] at the end. Oh.

Remi: 00:14:05.419 Yeah [laughter].

Marc: 00:14:09.189 Okay. So what would you think about a sort of historical structure of this week? Like first we say, "What is the free software perspective?" With the history of maybe Richard Stallman ranting about printer drivers, and saying that it's his printer so he should be able to read and modify the code of the driver, or stuff like that. And then introducing in a second part the open-source movement with Eric S. Raymond, basically saying with The Cathedral and the Bazaar, that the bazaar is actually a good way to do stuff. There can be several ways of structuring this kind of knowledge. And I'm not completely sure it's the most efficient way to do it historically.

Xavier: 00:15:17.804 The historical approach is kind of the traditional approach with this. I mean, it's not a bad way to do things. And I'm not sure I have a better way to suggest. One thing that I like to be conscious about is, how do we pique the interest of people who are getting into the MOOC? Because I mean - as you already know quite well, Remi - unlike a university lesson, you don't get a captive audience. So the very first lessons are actually quite critical in terms of how many people are going to stick longer. So, yeah. That aspect might be worth-- one point that could potentially help with this is, what's in it for the person who joins? A bit like what we mentioned. Why would they want to do this? So I don't know how to term that exactly. But if we could make that something personal about-- Okay. What are some softwares that are important in your life? What are things that you use on a daily life, and you'd like to know the people who do it a little bit better. To say, "Thank you," to them. Or even be able to add a feature there that you like. Going directly to the motivation of the person. And linking maybe, "What is free software," to that motivation, rather than just to Stallman's, for example. I don't know.

Remi: 00:16:58.633 Right. So, yeah. What I forgot to mention is that, of course, we don't have to follow the order from left to right. Of course, we don't have to start with the transmission part, and then interaction, and then activities, and then evaluation. Of course, we can start with activities, as you suggest, Xavier. So activities would be, of course, to ask everything that you asked. Like, "Do you use FLOSS?" "Is it important for you," etc. "Introduce yourself." So yeah. I was just asking about transmission. But of course, I think it's very important indeed to start with something catchy. Yeah. So maybe an activity is better because you are more involved. Also, if we have a very big person, star, in FLOSS, and we start with an interview, maybe that's something else that could be catchy. What other ideas you have?

Xavier: 00:18:27.118 The interview is definitely a great one. And especially if we can give you the feeling that there will be little bits and pieces of random famous people throughout the course. It's like it creates the incentive. "Okay, let's go see what else is there afterwards," and etc. So that could be nice. Yeah. For sure.

Remi: 00:18:46.975 All right. Do you have any names? So we were talking about Richard Stallman, of course.

Xavier: 00:18:53.330 We can try Stallman for sure.

Remi: 00:18:56.112 He wants to [offer too many?] - [crosstalk].

Marc: 00:18:58.108 Stallman starts to be a bit controversial in the FLOSS community since the Epstein affair I think.

Remi: 00:19:07.191 That is right.

Xavier: 00:19:09.626 Well, at the same time, he hasn't done anything in that thing. Yeah. But that's I guess the debate. But even beyond that, one thing that I'm thinking is that, for people who are very much into free software, Stallman is great. We want to watch him and hear him. But actually at the same time, if people are already in the free software community, they might not need that course, and especially that section, that much as the other people. What could be I think interesting is someone who has an appeal to people even beyond that. I mean, it doesn't mean that it's not someone from the free software community, but--

Marc: 00:19:59.096 Maybe we can ask Karen Sandler? Because she has a personal story about how she has a pacemaker, and she wants to see the source code of that to make sure she won't die, basically. And she's very involved in the free software movement, the SFC.

Xavier: 00:20:23.343 It could be. But I think we know her because we are in the free software community also. I mean, which doesn't mean that it's not a good thing to interview her. I think we need both. But what I'm thinking is, I mean even Torvalds is still pretty free software - or at least open-source, I would say - but the people outside of the community know him.

Remi: 00:20:51.604 Your [crosstalk] Linus? Linus Torvalds?

Xavier: 00:20:54.803 Yeah. And also I think what could be interesting in that, is that if we interview people who run big projects, we might also have different perspective on how a project is run. How they handle contributions, new contributors, older contributors? And definitely Torvalds has a pretty unique approach to management. I would say, not quite unique, but at least it's quite emblematic of the [inaudible] version of it. So that could be one. You could have people who are more representative of something more like [inaudible]. I don't know. And maybe also people who are outside of the community or joined it more recently. I think about, for example, Jeff Atwood from Discourse. He's known because of stack overflow and etc. So that's way broader notoriety there. And he definitely runs, quite successfully, a pretty big software project now.

Remi: 00:22:01.725 Can you put the names? Because I don't know him.

Xavier: 00:22:04.607 I'll put [crosstalk]-- I've put them in the beginning of the-- [laughter].

Remi: 00:22:08.583 I write everything at the end. And you write at the beginning.

Xavier: 00:22:11.747 So maybe we should merge.

Remi: 00:22:14.949 And what about very big names? Like-- I don't know. I was thinking about - even it would be a controversy but it's very interesting - Microsoft. Because Microsoft is-- it looks like what they say is that, it is the most active contributor on GitHub. Of course, GitHub is Microsoft's now. It is one of the biggest anyway, in terms of quantity of code they contribute too. So I don't know if someone from Microsoft is responsible for this open-source--

Marc: 00:22:51.905 There is probably an open-source program office at Microsoft.

Xavier: 00:22:58.705 It's good to also have that perspective, to show that it's not just one approach to open-source or free software, it's that there are different way of taking it. And regardless of your religion and political affiliation and economic sensitivities, there is something to dig into to open-source and free software. So I think that that could make sense. Yeah. Another type of personality could be people who are not necessarily into free software, open-source, but have done things that are close to it. Like for example, Carmack, John Carmack, doesn't really do open-source. But he has released a lot of his games, at least the engine, as open-source, afterwards. He's always been into mods and stuff. He's definitely someone people would really want to see talking. So it could also be asking them, what do you think about that then? I mean, even if they are not leading a free software project, they might have something interesting to say about it.

Remi: 00:24:07.148 He's working for Facebook now [crosstalk]--

Xavier: 00:24:10.814 True.

Remi: 00:24:11.614 --at Oculus VR.

Xavier: 00:24:15.464 Yup, yup. I mean, he's definitely not on the community side of things, I would say on this. But again, it's like, we are not asking him to give us the truth. But I would actually be quite curious to know what he has to say about free software, contribution, community members on the project he has led. I think that would probably be interesting.

Remi: 00:24:45.291 All right.

Marc: 00:24:48.936 So do we want to define now what we would ask people in interviews? Like, "What is free software for you? What benefits do you see in it," or--?

Xavier: 00:25:09.576 I mean, probably there are questions we can ask to them that will be related to all of kind of the categories that we have thought for this. And actually, one interview could be split into little pieces that come to you straight. Some parts of what we are saying, whenever there is something that match the stuff we want to pass on. Getting someone really known to say that, even if it's from a different perspective, could be interesting. So there could be somethings about "What is free software," but also, "How do you see contributors or mod-makers?" Or, "Do you think a collaboration happens on creating software?" These kind of things.
[silence]

Xavier: 00:26:12.741 Maybe asking them, "Do you have advices for developers joining a project? [How do you view contributors?]?"

Marc: 00:26:19.870 "Or students." For our students.

Xavier: 00:26:22.720 "Or students"? Yup. Exactly.

Remi: 00:26:31.123 Maybe they have a storytelling about how they get into it, in their company respective companies.

Xavier: 00:26:42.806 True. Yeah. Like, "Have you ever contributed yourself?" Especially the ones who are not free software leads. I'm pretty sure that John Carmack has already done open-source contributions to projects. Like, "How was it for you? Was it easier because you were John Carmack trying to send them merge requests, or did you also have to convince people?"

Marc: 00:27:12.537 Who is John Carmack [laughter]?

Remi: 00:27:16.222 John Carmack, it's Doom, Quake I think.

Marc: 00:27:20.985 Okay.

Xavier: 00:27:21.313 Yeah. Quake, id Software, these kind of things.

Marc: 00:27:26.248 Okay.

Xavier: 00:27:28.534 One of the reason I mention him, even though he's not really free software, is that whenever he gives a talk somewhere, or just open his mouth anywhere, it's guaranteed hundreds of thousands of views and people. Because he's really interesting. Like he sees everything in a very geek way. And yeah. He sees a lot of the stuff before they happen. So I guess people like that.

Marc: 00:28:00.549 Okay.
[silence]

Remi: 00:28:23.695 Polyèdre, what would you like to know about people that already have experience in contributing to FLOSS?

Polyèdre: 00:28:37.741 Maybe more testimonies. Or what they did in order to know some names of FLOSS maybe. And maybe how they began contributing.

Remi: 00:29:09.209 Okay.

Xavier: 00:29:10.892 Yeah. I think that one is a really interesting one. Because I think for a lot of people, me included, before doing the first contributions it always feels a bit ephemeral. What do you actually do? What is it? And I think everyone shares a little bit of a fear at the beginning. So hearing a big figurehead saying, "Yeah. My first contribution, I was like 35. And it took me two months before I actually clicked send." I think that that might help also, to pass a bit that part.

Marc: 00:29:42.476 My first contribution took like two years to write [laughter].

Remi: 00:29:47.233 for you, Marc?

Marc: 00:29:48.623 Yeah.

Remi: 00:29:49.451 That's interesting also [laughter].

Marc: 00:29:51.993 Well, it depends. I reported a bug at some point and I wanted to fix it. And it took me-- I did other things and then came back to that two years later.

Remi: 00:30:03.865 Ah. For me, it took me four nights I think. Whole nights. Like white nights, as we say. I wanted to fix that bug because I really wanted the software to work as I wanted, so.

Marc: 00:30:35.561 Yeah. I think knowing that from others definitely helped. I think one of-- sorry. Go ahead.

Remi: 00:30:44.147 The fear to start contributing, I don't get it quite well. Why would you-- from what? The fear from what? From publicly doing something that is wrong, or--?

Marc: 00:31:04.038 Publicly doing anything is scary [laughter].

Xavier: 00:31:07.876 Yeah. It's a bit the imposter syndrome I think, is that, "If I post that publicly, everyone will see that I'm not such a great coder after all. Those guys are gods." There is that aspect. And even just the publicness of it, even independent of the code, is that you have to put yourself forward, in front of everyone. I mean, it depends on the personality. Some people have no problem with that, but I think most personalities who tend to come to code are not that outspoken from the get-go, so.

Remi: 00:31:45.132 That's interesting.

Marc: 00:31:49.566 And when you put something on the internet, your audience is the world basically. Which might shift in mentalities somehow. I don't know. It's my impression that people are more used to post things online now. But I'm still terrified any time I-- [laughter]. And I don't have a blog now [laughter].

Remi: 00:32:20.464 And Twitter. Twitter is also like the core of this public. So anything you do on Twitter, you could have what is called, "A shitstorm," [laughter]. I don't know if you know this wording, But--

Marc: 00:32:38.898 MM-hmm. For sure.

Remi: 00:32:41.243 Yeah [crosstalk]--

Xavier: 00:32:42.556 And actually, when we think of activities for this week, maybe that could be a good way to start getting into the final project that we mentioned. Could be good to start to introduce early is, even before starting to contribute something like code, or even a typo and etc., I think that first step of coming to a project and saying, "Hello. Thank you for the software that you've done. I love it." Which is not something-- I don't think anyone will complain about any spam from that. Of having thanks. So maybe that could be a first thing, is to pick two of your most favorite projects and go say thanks to the developers or something. And that's quite a scary step I think [inaudible].

Remi: 00:33:32.247 Ah. That's interesting.

Marc: 00:33:33.304 Yup. As long as, not everyone takes the same project and-- [laughter].

Xavier: 00:33:37.247 Yes. I guess.

Remi: 00:33:38.613 [crosstalk] Firefox. Oh.

Marc: 00:33:41.316 It's hard to contact Firefox.

Remi: 00:33:43.178 Yeah.

Xavier: 00:33:55.819 And I guess for popular projects, we could always think about using the forums that people do. Like kind of a group post, or something. But even that, I'm not completely sure it's necessary. Because I have a hard time imagining any of the project where I've contributed, where there will be suddenly 50 people coming and say, "Thank you. That's so amazing. I love it every day." And seeing anyone complain about that. If it's true, "Thank you", not just like a spam. I mean, maybe I'm wrong, but I would like it at least.

Remi: 00:34:28.912 Yeah. Yeah. 50 is Okay. 1000 over 20 days would be-- [laughter].

Xavier: 00:34:37.493 Do you think there will be that many people following the MOOC? Or how many people do you expect?

Remi: 00:34:45.639 I have no clue; I don't know.

Xavier: 00:34:51.216 I mean, maybe people wouldn't like it. But first, I'm not convinced of that. But that's also, pragmatically speaking, a way to attract attention on the MOOC, no?

Remi: 00:35:02.170 Yeah, yeah, yeah. And that would bring feedbacks saying, "Oh, this is amazing. Thanks for sending those people." Or, "Please stop [laughter]."

Xavier: 00:35:13.893 Yes. And in which case it's fine. Like, "Sure. We'll stop that next time."

Remi: 00:35:23.131 I was thinking about something happening frequently for FLOSS, is that people are complaining. You mentioned that, Xavier. Just arriving without maybe even a chat or support, and saying, "Oh, this shitty software doesn't work. How do you make it work?" Really aggressively. Because I saw that many times. They just want a bug to be fixed quick. And they think that it's easy. They have absolutely no clue about the value of the software they are using. Oh. The value is also something very interesting. And they just come, and they think that we should fix the bug as soon as possible because it's very important for them [laughter]. Even if the bug is here for three years, they don't want to contribute. They just want to cry and to complain.

Xavier: 00:36:42.482 Sure. And I think often-- I think the worst I've seen that is kind of the outrage at the fact that they even have to complain about this. How is it possible that this even exists here? And yeah.

Remi: 00:36:58.572 Oh, and so that reminds me. The feeling of being a contributor to an open-source project, and having all this negative feedback sometimes. And I remember that some of the contributors said, for them it was difficult to accept this kind of feedback. So it's really psychologic.

Xavier: 00:37:32.190 Yeah, it is. And I guess that would be nice to have a bit more of the other side sometimes. So but are you saying that as something to describe, to address in the course? The fact that people complain a lot and don't say "thanks" enough or something?

Remi: 00:37:53.760 I think this is happening. I'm pretty sure we can find resources online that have a very small story about this kind of stuff, that we could add. So that you have a better understanding on what's going on in FLOSS communities. Many things can happen. It's public, and many people don't understand FLOSS. They just use it. And they don't know that many people are contributing [inaudible] for free. And yeah. I think it's important to introduce it.

Xavier: 00:38:39.687 True.

Remi: 00:38:48.815 I was talking about the value--

Xavier: 00:38:50.187 And actually, might be worth also warning the students, future contributors, that this goes both ways. Is that when you try to contribute, sometimes you're kind of not very nicely treated by some projects. And might be good to be prepared for that, and not take it too personally. Because, yeah. You have really amazing projects, amazing maintenance. But sometimes you have like-- people will--

Marc: 00:39:18.092 You have burnt-out maintainers. And burnt-out maintainers are not nice [laughter].

Remi: 00:39:25.520 Interesting.

Marc: 00:39:26.915 That was actually the topic of a talk at FOSDEM this year, that burnout-- you don't want to keep burnout contributors, for their sake and for the sake of your potential new contributors.

Xavier: 00:39:42.209 For sure.

Marc: 00:39:52.273 So should we start to structure the things we will do in the MOOC? Like as actionable steps to create stuff, or continue to brainstorm and find things to talk about you? Remi, you are the master of the time here.

Remi: 00:40:13.919 Yeah. We have 30 minutes left. But you mean the structure of the first week?

Xavier: 00:40:21.714 Yup.

Marc: 00:40:23.487 Yes. We give a lot of ideas and things that we could do. But it's not really a list of steps that the students would go through.

Remi: 00:40:36.317 I don't know. I would prefer to continue brainstorming to have more ideas. And maybe the structure will come later, I would say. I don't know. Because--

Marc: 00:40:51.517 You're the MOOC expert [laughter].

Remi: 00:40:53.773 I know, I know [laughter]. So we had some ideas with Xavier and Polyèdre for activities. Saying "hello" to a project. But that would mean that you already know a project, or do you have one in mind?

Marc: 00:41:22.620 And also, what I mentioned in the pad is that it kind of depends of stuff that we'll mention in week 4. Which is, where to find people involved in a project? Because if we say, "Go say thanks to your favorite project," and your favorite project is Firefox, it's like, "How do I say thanks to Firefox?" And that's a very hard question actually.

Xavier: 00:41:52.822 Yes. From my side, that's exactly why I'm interested in That. It's because it allows to not be just theoretical at the beginning, but also have already kind of that social aspect. So that might mean moving some stuff around; I don't know exactly how. But I think it would be valuable for people to start digging into this kind of stuff. Because if we just tell them what is free software in general, and then they have to wait until week 4 to actually get in contact with anyone, I think we'll lose a lot of people in the meantime, I think.

Marc: 00:42:41.372 Maybe.

Remi: 00:42:41.993 Xavier, so I understand that your pedagogy is more, do some activities, be active-- what do you call it? Active pedagogy, right?

Marc: 00:42:57.639 Maybe we can ask for a public action. Like, "Tweet your thanks to some project." So that if you mention Firefox, then people who is from community [mentioned?] also, I think, will see it. And it won't require to get into-- or make a blog post about why you think that your favorite software is awesome.

Xavier: 00:43:24.972 Yeah. And I mean, that can be a good default action for people who don't know how to contact the right person. And also, let's not forget that because the course is accessible freely, we can always have a link. Like by default to just tweet and etc. To have something easy for everyone to do. But there can always be a link to the section where we describe how to contact people. And if people want to be more involved, they can already look at that. Doesn't have to be completely [inaudible], that's what I meant.

Marc: 00:44:00.392 Yes. The kind of things I'm thinking about was the I-love-free-software day, or things like that. Where there are call to actions, to publicly say thanks to free software projects.

Remi: 00:44:12.520 Oh, that's nice; I like it.

Xavier: 00:44:15.626 It's in three days [laughter].

Remi: 00:44:20.938 Okay. Can you put the link in the Framapad? Because I cannot copy/paste things. This is crazy.

Xavier: 00:44:29.345 That's nice, this one.

Marc: 00:44:41.988 So that could be some call of actions that does not require to know where to find communities. Or we can tell them, "If you don't want to just say thanks, you can write a tutorial, so that people can understand your use-case of some softwares that you're using. Because writing resources is a great way of contributing without saying code.

Xavier: 00:45:10.379 That's true. That's actually often one of the first thing I do on Project I get to, is that I write some documentation because it's always needed. You don't need a lot of approval. And, yeah. Agreed.

Remi: 00:45:24.488 Would that make sense? To have a kind of-- during the whole MOOC, a kind of database of projects, of communities, of institutions?

Marc: 00:45:40.734 Yeah, yeah. Definitely.

Remi: 00:45:43.230 So. If we have this kind of database, would it be possible to make it, first of all, static and easy to contribute to and searchable?

Marc: 00:45:54.141 You mean a CSV in GitLab?

Remi: 00:45:59.356 Yes [laughter].

Marc: 00:46:05.116 A CSV in GitLab is editable and searchable, and easy to edit.

Xavier: 00:46:11.436 Actually, that's true. That's a great way. Maybe we're [getting ahead?] of the use of GitLab, and merge requests, and etc. But that's definitely a contribution to the course that would fit very nicely into the flow of the course, without having to go on an external project immediately. Just like list the project used, do you know where to contact-- yeah, put it there. Do you know if they like contributions or not? Have you contributed something in there? We could also keep a repository of what we generated as contributions with these [words?]. Yeah, great idea actually.

Remi: 00:46:49.917 Okay. Yeah. So we could start with activities that would involve contribution to the content? Right at the beginning?

Marc: 00:47:00.128 Yeah. So that would be an optional activity, right?

Remi: 00:47:05.107 Yes.

Xavier: 00:47:06.428 But why optional?

Remi: 00:47:08.035 Ah. Or even it could be evaluated. [crosstalk].

Marc: 00:47:14.732 It's true. You see the difference between optional and it's evaluated. Yeah, I guess it could be both. I think in case, it's worth asking everyone to do it. Then, of course, evaluation, that might be another matter, to figure it out. But, yeah. In any case, I think it makes sense.

Remi: 00:47:40.060 Interesting. Okay. Yeah. Because in the activity, we have to find-- this sentence. "Find many examples of free software. And try to find users, devs, etc. So maybe we can dig on the system to facilitate the contribution to this list. All right. I saw many projects on GitHub, like - how do they call it - awesome list. And there is also the awesome of awesome, like the list of all the awesomes. And some of them, they have a very nice way of listing things. Even you can order on each column like a database. And there is a little search bar. Everything is client-side. I don't know how they do that, but we could just take one of them that works.

Xavier: 00:49:05.464 Yeah. For sure. Actually, it could be also worth checking if there are already lists of free software projects, and where to contribute, and etc. Because, yeah. Even if we don't want to keep that or contribute to that specifically, if the list is under a free license suite, could be already a good base, so that we don't start just from zero.

Marc: 00:49:31.095 Yeah. I think there are such lists.
[silence]

Remi: 00:49:43.382 Yeah. I just put one link; awesomelists.doc. I'm not pretty sure the source is static. I don't know. But there is a search bar. And there is even awesome tag, I think, on--

Xavier: 00:50:15.412 But it's not just about FLOSS, right?

Remi: 00:50:18.041 No, no, no, no. It's not anything. Anything that people think is awesome, and they want to share it to anyone. And then you have categories of awesomeness. Maybe there is a awesome FLOSS project list.

Xavier: 00:50:38.446 Yeah. That's what I was mentioning before. Awesome open-source. I'll copy it. I've put it there. But I think it's like any-- well, they have, they claim, 300,000 projects.

Remi: 00:51:09.567 Oh, I know this website. And I've been to this website many, many times to find projects. Yes. This is a really great and really big list. And I think you can contribute to that list, but I don't remember how.

Xavier: 00:51:31.712 Yeah, they don't have it. Yeah.

Remi: 00:51:34.114 No, they don't say anything on the website. Also, there is this awesome open-source link on GitHub.

Xavier: 00:51:55.616 Yup. I'm seeing that one. I don't know if that's the same. That's probably a separate list.

Remi: 00:52:01.824 Yes, it is.

Xavier: 00:52:09.684 But I mean, we could always take either of those and-- because the intent of our list is oriented toward contributions, and contact with developers, and etc. So we would probably very quickly diverge from the goals of the existing ones. Unless there is an awesome contribute to FLOSS. But, yeah. If not, then we can actually base it off that, create awesome contribute to FLOSS. And then use the course to keep populating it.

Xavier: 00:52:43.388 Yeah.

Remi: 00:52:49.232 All right. We can also link to generic lists of FLOSS projects, so that people can just find them and see if information is still up to date, about whether it's easy to find or contribute to--

Xavier: 00:53:04.595 True. Yeah, a link to the awesomeopensource.com one, given that it's so big, probably is good. Even if our list, at least at the beginning, is much smaller.

Remi: 00:53:17.441 And maybe catalogs of applications, open-source applications, like F-Droid on Android.

Xavier: 00:53:28.476 True. So sites like AlternativeTo, that's often what I use when I want to find--

Remi: 00:53:40.147 Yes.
[silence]

Xavier: 00:54:02.625 And actually, because we are in this section, "What is free software," which is maybe the most generic of what we do, are there any existing courses we could use for that? Like materials maybe from other places?

Remi: 00:54:18.049 Courses--?

Marc: 00:54:18.874 Pretty sure there are many materials about that on the FSF website, for instance.

Remi: 00:54:26.146 Even you have some links on awesomeopensource, this GitHub link I gave. Like governance, licensing, legal [couture?], organizations, funding conferences, community. Research surveys, best practices.

Xavier: 00:54:46.961 Oh, and [inaudible] is mentioning framalibre.org also, of course, that's also a good one to--

Marc: 00:54:55.647 The main philosophy page of gnu.org starts with a "What is free software," paragraph.
[silence]

Remi: 00:55:15.897 But indeed meta-list, I mean, or big list is always a good starting point for students in this MOOC to [search?] and navigate through.

Marc: 00:55:29.247 It can be tricky. Because if they have no idea what they want to contribute to, they can be lost in such a list I think.

Remi: 00:55:39.250 Maybe also give some popular lists. Like the top 100 FLOSS projects. Like the top popular, something like that.

Marc: 00:55:53.339 Like the SILL?

Remi: 00:55:56.495 Yes [laughter].

Marc: 00:55:58.753 SILL is very French centric, sorry [laughter]. But I'm not aware of an international alternative of this kind of list. But probably exist.

Remi: 00:56:23.354 The really big list of really interesting open-source projects. From 2016 [laughter].

Xavier: 00:56:38.334 And also, besides the list, to come back to the activities quickly, because I just remember something. In Upstream University, there was something that was nice, I find, for introducing the idea of free software and contributing using Legos. Where basically someone was constructing something, and someone else came with an idea of how to modify it, and turn - I don't know - a house into a building. Or like add something in there. And generating that interaction of, "I create something, you come to add to it," to show some of the very basic things without getting too technical. I think at some point there was an idea to also translate that online into Minecraft or free craft, the free software equivalent. Building something in a corner, and then coming to add to that. Maybe there is an activity around that that could be interesting? Just to show the base mechanism of what we are talking, even without getting too deep.

Remi: 00:57:52.603 Do you have a link for this? Because you mentioned that many times. [crosstalk]--

Xavier: 00:57:58.389 No. That's a bit the problem is that a lot of those things were done in person. So there are not a lot of online resources about it. I've been discussing with Loic-- by the way, I don't know if you have a chance to look at it. But we exchanged a bit on that. So it might be good for you to comment on that one. But what Loic was suggesting was to maybe redo once that training that he was doing. But do it recorded this time. So that we can take inspiration from whatever that was done there. So I don't know if that should be toward one of us, would be following Loic. So I don't know. Polyèdre, maybe if you are willing to get a personalized training by Loic, that could be an interesting idea. And yeah. And trying to see how that would work. But yeah. I see that you've got a link already.

Remi: 00:59:07.690 Yeah. It's the archive of Upstream University, two thousand-- I don't know. And indeed, I can find the exercise, "Lego applied to free software contributions," 15 minutes. And even they had a two-hour contribution simulation. Very interesting. 2020; it was last year.

Xavier: 00:59:32.818 Yeah. And also, it's also, really give some fun activities from the get-go, where it's-- yeah. It's something almost game-like, to do immediately. Normally people respond quite well to that.

Remi: 00:59:52.207 Okay. Yeah. There is a link, Marc, and then [crosstalk]--

Marc: 01:00:04.660 Yeah, yeah yeah. I found the link.
[silence]

Remi: 01:00:24.147 So should we simulate a contribution? The simulation could be, you contribute to the course content itself.

Xavier: 01:00:35.525 It could be. I think in any case, contributing to the course content itself is a great step. And I think we should avoid that in any case. I think here there is-- maybe the difference would be that it's like something very light. People just create something that they care about for 15 minutes. But it's not something on which there is a deep need to have something curated, and have good moderation, and etc. It's more of a game, of a simulation. And it just helps them to understand. To put themselves also under the skin of the maintainer, who created something. There is someone coming to put these crazy Legos on top of it. There needs to be a process to that. You can't expect people to just accept it. And I think that that's kind of the main point there.

Marc: 01:01:31.235 Yeah. And it's actually something - especially in Minecraft servers - that is kind of understood. On many public Minecraft servers, there is a process to contribute, basically. I've interacted with a few Minecraft-- like a lot of Minecraft in the distant past, and few Minecraft players recently. And there is always similar issues as in FLOSS. Like permissions issues, trust issues. And basically, the fact that when you have 10 people contributing to build a cathedral in Minecraft, it goes much faster than when you have one [laughter]. So for people who know building games, it can be something that is quite intuitive and quite easy to understand I think. I don't know.

Xavier: 01:02:44.068 Well, I think that's the point. Is that a lot of the people in those trainings, they don't really play Legos that much, or Minecraft, or whatever. But by putting them in that situation, and at least orienting it to that, it tells them to experience that in a way that, without the game, would be a bit more fuzzy and theoretical, basically. So, of course, it requires to put the right circumstances. And I guess the training, in the case of Upstream University, the big advantage of being in person for the most part. With small groups, with a mentor. So you get a lot of structure that is a lot more difficult to replicate in a MOOC. But if we could find something that takes that principle, and adapt it to the process, I think it would probably be-- yeah. A good point.

Remi: 01:03:40.387 So I really like the idea of [crosstalk]--

Marc: 01:03:43.648 We could host a Minecraft server, but-- [laughter].

Xavier: 01:03:46.673 Yeah, we should.

Remi: 01:03:49.759 Okay. Do you have Web-based clients for Minecraft? Or do you have to install a heavy client?

Xavier: 01:04:05.200 Minecraft is proprietary.

Remi: 01:04:07.634 All right.

Xavier: 01:04:09.153 But you have free craft or-- how is it? Maybe it's actually called open craft. I might [crosstalk] - [laughter]. I remember there was a conflict when we chose the name. So that might confuse people if we do that. But I think that it--

Marc: 01:04:26.247 Minetest. Minetest.

Xavier: 01:04:27.625 Okay. That's good then, even if it's not the same name. But I'm pretty sure there is at least some kind of open-source alternative. And since we don't really want to build big cathedral walls, having a lot of players, we just want a small expense, maybe that's enough. I don't know.

Remi: 01:04:47.148 Oh, that's very interesting. I really like the idea of gamification. Of using a game at the beginning and do a little contribution in the open game. That is nice.

Xavier: 01:05:06.536 It has a heavy client; it's not web-based.

Marc: 01:05:10.372 Minetest?

Xavier: 01:05:12.030 Yeah.

Remi: 01:05:14.145 Heavy like what? Oh, and there is an Android client. Windows, Android, Linux, FreeBSD. Nice. And even Mac. Interesting. So how big is it? 18 megabytes? It's not really big. But then maybe they have to-- if it is in 3D, the quality of their hardware.

Xavier: 01:06:01.308 It's true. Because we were talking about people on phones and stuff. I mean, maybe there is a phone version even, no? There is one for Minecraft.

Marc: 01:06:09.971 There is an Android version.

Xavier: 01:06:12.456 Oh, yeah. Perfect.

Marc: 01:06:20.012 But yeah. Since it's an open building game, we could lose people, just to the game [laughter].

Polyèdre: 01:06:30.343 It's better to lose them to the game than from boredom I guess, so.

Remi: 01:06:38.396 But Framasoft created this game, Framinetest Edu.

Marc: 01:06:48.060 Isn't that a Minetest instance?

Xavier: 01:06:52.610 Yeah, it's an instance. Yeah, because you're on the education page, right? Yeah, I went there too. Yeah, yeah. That seem to be their instance, which makes sense that Framasoft would host one of the servers, I guess.

Remi: 01:07:08.074 Yes.

Xavier: 01:07:10.447 We could use that potentially. That sounds great actually.

Remi: 01:07:16.868 So we could use this one.

Xavier: 01:07:20.001 And it's even meant to be used in classrooms. So I think we are quite on-topic there.

Remi: 01:07:24.207 Yes, yes, yes. Indeed. Ah. That's nice. Then you would have to access a specific port. Looks like the port is 30000.

Xavier: 01:07:48.709 Yeah. For games, you usually require a specific port.

Remi: 01:07:53.042 In our school, it's forbidden to use weird ports like this.

Xavier: 01:08:01.860 Or any UDP.

Remi: 01:08:03.967 Or any UDP, yeah.

Xavier: 01:08:06.391 Yeah. Because schools generally don't want people to play weirdly enough.

Remi: 01:08:10.559 That is right. Yeah. We want gamification [laughter]; it's incompatible. That's an idea. I can see that time is running; we have five minutes. But we do have great ideas anyway. Do we have very important points that we have to cover in five minutes? So if I go to the agenda today--

Marc: 01:08:38.541 Well, we have almost all the agenda points that's not really explored. Like the first question was, "What are the cultural things people wanting to interact with FLOSS projects will need to know? And what are the main ideas we want to convey in this week? Where to draw the line with week 3?" To not have duplicated stuff, even if it's possible to have. And yeah. We also need to introduce the main project of the course, the contribution, to the students, so that they start thinking about it. Which is something that we mentioned yesterday. And well, finally, "How do we organize this week? Which blocks, and which content in which block? And which questions, in multiple-choice questions?" Which are all big questions.
[silence]

Remi: 01:10:00.595 I think we do have-- Xavier gave a lot of ideas indeed. The main ideas to convey in this module. And I think, indeed, to understand the mechanisms of contribution using a game. So the solution of using a game is great. But the main idea is to understand contribution and how it works. Maybe it's not to define everything, but to really have the idea of the contribution by doing, right?

Marc: 01:10:49.151 So maybe the game is more central to the idea that we want to pass on?

Remi: 01:10:56.440 Looks like yes.

Xavier: 01:10:58.415 Well, I think at least as a starting point. But then I think also we can still properly introduce the Four Freedoms, or free software, and etc. But if we start from that, from what's at the core, what we want to be able to do, that allows to explain why we need those four freedoms. You can refer to the game. Yeah, when you were trying to change someone else's, or improve someone else's construction, if you could copy it, so then you were solely relying on what that person was doing, and etc., etc. Basically, using that complete experience to illustrate the things that might be a bit more theoretical with free software.

Marc: 01:11:54.148 Yes. So the equivalent would be basically freedom to play? Just to move around? Basically, to visit the world, to explore? Freedom of looking how things are done? Basically, how others build their stuff? Freedom to build? And freedom to have people visit your stuff?

Xavier: 01:12:25.370 Well, I think it might be-- if you think about objects, that might be easier than the whole world. Is that, if someone - I don't know - builds a chair, so the freedom to use would be to make a copy for you of that chair. You can, because you're in a virtual world. Then you can use it without having to ask the permission from the guy who designed it originally. So I think with an object, it's a little bit more direct actually, what you can do. Because you can take it into your own world. It doesn't have to be someone else's place, which is always a bit weird. People invite themselves to your place all the time. So yeah.
[silence]

Remi: 01:13:22.345 We have to make sure it's easy onboarding also.

Marc: 01:13:32.890 True.

Remi: 01:13:34.474 So we have to test something. We have to test if it is easy to install. So, for example, I just installed the game Minetest, I opened it, and I am in the game right now. So it took me like one minute. Looks like it is working.

Xavier: 01:13:49.582 Cool.

Marc: 01:13:50.355 Yeah, same for me.

Xavier: 01:13:54.479 Oh, so that's why you're not speaking anymore. I've lost you. You're playing now. All right [laughter].

Marc: 01:13:58.326 No, I closed it.

Remi: 01:14:01.043 So I tried to just navigate into the game, but I couldn't with my keypad, so. I mean the arrows. So we'll have to check and do a little test here [crosstalk] feasibility.

Xavier: 01:14:19.090 And also like figuring out what exactly we want people to do. I think it would be good to talk to Loic about this. Because I know that he's made some tests. So he had actually some concrete experience with the students on this. But I don't remember what he did, because at the time when I was involved it was still more the Lego thing. So yeah. That could be a good question to ask him.

Remi: 01:14:46.302 That was interesting. So it's already two minutes past 8:00, French time. So it's already the end of this brainstorming. Very interesting. So what I suggest is we conclude already. I'm sorry. Because we could do that for hours, I can see, and get like thousands of ideas. So [crosstalk]--

Marc: 01:15:13.380 Should we have action items or--?

Remi: 01:15:20.156 Yes.

Marc: 01:15:21.776 Maybe or even another video conference about this week or--? Or action items to do before that or--?

Remi: 01:15:36.252 I like the idea of the game. So maybe the action is to--

Marc: 01:15:42.750 To test?

Remi: 01:15:44.029 To test. And to decide if we indeed have a game at the very beginning. The feasibility of everything. We have to make sure that they-- of course, the game is great. But we have to make sure that it's not an obstacle also. Right. But very interesting. So, yeah. This is one action. And the other is-- well, we have to gather everything that we wrote in the pads and make it more structured in an issue, I think. I'm not pretty sure if we need another brainstorming for this specific week. What's your opinion--?

Marc: 01:16:36.471 Before having tested if a game can work that settings, maybe having discussed with Loic?

Remi: 01:16:44.377 Yes.

Xavier: 01:16:46.234 Yeah. I can take an action item to bring that up with him, and ask him for our present experience with this.

Marc: 01:16:52.614 Especially, can it work without being mentored during the play?

Xavier: 01:17:00.962 Yes. That's definitely an important question.

Marc: 01:17:05.893 Another thing I think, as an action, is to collect a lot of links. Links of lists, of projects. We already have some of them. And maybe after, when we have a kind of bootstrap list, we can prioritize the list somehow.

Xavier: 01:17:29.664 I think I already had some in...

Remi: 01:17:37.701 The root of the repository?

Xavier: 01:17:41.445 No.

Remi: 01:17:43.597 In the scenarisation resources.

Xavier: 01:17:50.389 Here? Here there is already a list of FLOSS projects. Which we could improve on.

Remi: 01:18:03.102 It's a list of FLOSS projects.

Marc: 01:18:06.986 Yes. It's a paragraph of the lists of FLOSS projects, so it's a list of lists of FLOSS projects [laughter]. So there isn't any history in Wikipedia about articles-- there is a category of lists, and there is a category, lists of lists. And basically, the list of lists of lists is not existing, and protected, so that people cannot create it. Because people keep trying to [laughter].

Remi: 01:18:49.198 All right. So well, thank you again, everyone.

Marc: 01:18:52.975 Thank you.

Remi: 01:18:54.661 Wow, that was fast. I mean, it's amazing. Because one hour and a half, and for me it looked like 20 minutes. It went very fast. I guess we have great ideas. And maybe important decision to make; more gamification from the very beginning. So--

Marc: 01:19:17.839 At least we have new ideas. I did not, at least, plan the gamification thing [laughter]. Or thought about it.

Remi: 01:19:25.983 That's awesome. You have awesome list [laughter]. Awesome. So next date is when we go to the issue of brainstorming of week 2--

Marc: 01:19:42.300 15, yeah.

Remi: 01:19:43.530 --[in half?] February.

Xavier: 01:19:45.740 It's about Git, and GitLab, and GitHub.

Remi: 01:19:48.598 All right. Also, there is--

Marc: 01:19:50.380 So we will have to invite Olivier Berger.

Remi: 01:19:54.449 Yup. A colleague at Telecom Paris. Well, I thank you all for today's brainstorming. That was very great, very efficient. I mean, in terms of ideas. And we'll have to structure everything and we'll do a report. Everything will be on the GitLab again. So thank you all for contributing again to this MOOC, and see you next time. And even before for some of you, if we meet with other meetings in between the brainstorms. That's it for today.

Polyèdre: 01:20:32.455 Okay. Thank you. Bye.

Marc: 01:20:34.709 Thank you. Bye.

Remi: 01:20:35.885 See you.

Marc: 01:20:35.885 Thank you, bye.

Xavier: 01:20:36.885 Good evening.

Remi: 01:20:38.474 Good evening.
